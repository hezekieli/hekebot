import subprocess, sys, random
from random import randint

def match(botA, botB, mapnumber):
    print ('java -jar tools/PlayGame.jar maps/map' + str(mapnumber) + '.txt 1000 1000 log.txt "java ' + botA + '" "java ' + botB + '"')
    # n, p = subprocess.getstatusoutput('java -jar tools/PlayGame.jar maps/map' + str(mapnumber) + '.txt 1000 1000 log.txt "java ' + botA + '" "java ' + botB + '"')
    pipe = subprocess.Popen('java -jar ../tools/PlayGame-1.2.jar ../maps/map' + str(mapnumber) + '.txt 1000 1000 log.txt "java ' + botA + '" "java ' + botB + '"', stderr=subprocess.STDOUT)
    # output = pipe.stdout.read()
    output = pipe.stderr.read()
    # sts = pipe.wait()
    # print ("sts = " + str(sts))
    # print ("output = " + str(output))
    p = str(output)
    
    #p = subprocess.check_output(["java", "-jar", "tools/PlayGame.jar", "maps/map7.txt", 
    #            "1000", "1000", "log.txt", "java " + botA, "java " + botB], )
    #(output, err) = p.communicate()
    #stroutput = "" + output
    # print (p)
    
    text_file = open("RunbotOutput.txt", "w")
    text_file.write(p)
    text_file.close()
    
    
    # print ("Map " + str(mapnumber) + botA + " vs " + botB)
    # print ("p = " + str(p))
    # if p.find("Player 2 Wins!") == -1:
        # print ("Player 1 Wins!")

    if p.find("Player 2 Wins!") == -1:
        print (botA + " won")
        return 1
        
    else:
        print (botB + " won")
        return 2                
        
def main():
    for argument in sys.argv[1:]:
        print (argument)
    
    if len(sys.argv) < 3:
        print ("At least two arguments required")
        return
    
    matches = [0.]*len(sys.argv)
    matchesWon = [0.]*len(sys.argv)
    '''
    a = randint(1, len(sys.argv) - 1)
    b = 0
    while True:
        b = randint(1, len(sys.argv) - 1)
        if a != b:
            break
    print a
    print b
    
    '''
    for map in range(1, 11):
        '''
        a = randint(1, len(sys.argv) - 1)
        b = 0
        while True:
            b = randint(1, len(sys.argv) - 1)
            if a != b:
                break    
        '''
        a = 1
        b = 2
        # v = match(sys.argv[a], sys.argv[b], map)
        # print (v)
        if match(sys.argv[a], sys.argv[b], map) == 1:
            matchesWon[a] += 1
        else:
            matchesWon[b] += 1

        matches[a] += 1
        matches[b] += 1

        
        for i in range (1, len(sys.argv)):
            if matches[i] != 0:
                print (sys.argv[i] + " Games: " + str(int(matches[i])) + " : win-loss: " + str(int(matchesWon[i])) + "-" + str(int(matches[i] - matchesWon[i])) + " : " + str(matchesWon[i] / matches[i]) + " % won.")
            else:
                print (sys.argv[i] + " no matches yet")
        
    
if __name__ == "__main__":
    main()