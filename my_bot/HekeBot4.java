import java.util.*;

/*****************************************************************
 * HekeBot2
 * Is amazing and amazingly simple! Like all the best things are.
 * 
 * It simply 
 * - sends 1 fleet per turn 
 * - from the strongest planet
 * - half the available ships
 * - if there's at least 50 ships
 * and to a planet with best score calculated according to
 * - closest distance
 * - least ships
 * - best growthrate
 * 
 * Problems ensue when there are not very high growth rate planets. 
 * Another thing to improve is accelerating the fleets sent when
 * gaining more planets.
 * 
 * @author hemijupe
 *
 */

public class HekeBot4 {
    // The DoTurn function is where your code goes. The PlanetWars object
    // contains the state of the game, including information about all planets
    // and fleets that currently exist. Inside this function, you issue orders
    // using the pw.IssueOrder() function. For example, to send 10 ships from
    // planet 3 to planet 8, you would say pw.IssueOrder(3, 8, 10).
    //
    // There is already a basic strategy in place here. You can use it as a
    // starting point, or you can throw it out entirely and replace it with
    // your own. Check out the tutorials and articles on the contest website at
    // http://www.ai-contest.com/resources.
    public static void DoTurn(PlanetWars pw) {  	
		
    	/* Log file Writer
    	PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
    	writer.println("The first line");
    	writer.println("The second line");
    	writer.close();
    	
    	
    	
    	/*
    	List myPlanets = pw.MyPlanets();
    	Collections.sort(myPlanets, new PlanetComparator());
    	
    	//List notMyPlanets = pw.NotMyPlanets();
    	
    	for (Planet p : myPlanets()) {

    	}
    	*/
    	
    	
    	// No point fighting if you don't have any planets left
    	if (pw.IsAlive(1) && !pw.MyPlanets().isEmpty()) {
    		
    		// (2) Find my strongest planet.
			Planet source = null;
			double sourceScore = Double.MIN_VALUE;
			for (Planet p : pw.MyPlanets()) {
			    double score = (double)p.NumShips();
			    if (score > sourceScore) {
				sourceScore = score;
				source = p;
			    }
			}
			// (3) Find the best target enemy or neutral planet.
			Planet dest = null;
			double destScore = 0;	//Double.MIN_VALUE;
			for (Planet p : pw.NotMyPlanets()) {
				
				//System.err.println("Planet " + p.PlanetID() + " GR = " + p.GrowthRate() + "\n");
				double o = 0;
				if (p.Owner() > 0) o = 0.75;
				else o = 0.5;
				double gr = (double)p.GrowthRate() / 5.0;
				double ns = (double)p.NumShips() / 50;
				if (ns >= 1) ns = 0;
				else ns = 1 - ns;
				//double ns = 1.0 / (1 + (double)p.NumShips());
				double d = (double)pw.Distance(source.PlanetID(), p.PlanetID()) / 10;
				//if (d >= 1) d = 0;
				//else d = 1 - d;
				d = 1 - d;
				//double d = 4.0 / ((double)pw.Distance(source.PlanetID(), p.PlanetID()));
				double score = (o * ns) + gr + d;
				
				//double score = gr / ((1 + ns) * (d*d));
			    //double score = (double)p.GrowthRate() / (1 + (double)p.NumShips() * (double)pw.Distance(source, p));
			    //System.err.println("Planet " + p.PlanetID() + " score = " + score + "\n");
			    if (score > destScore) {
				destScore = score;
				dest = p;
			    }
			}
			// (4) Send half the ships from my strongest planet to the weakest
			// planet that I do not own.
			if (source != null && dest != null) {
				int gr = 0;
				if (dest.Owner() > 0) gr = dest.GrowthRate();
				int numShipsNeeded = (dest.NumShips() + (pw.Distance(source.PlanetID(), dest.PlanetID()) * gr));
				if (source.NumShips() > numShipsNeeded) {
					int numShips = numShipsNeeded + 1;
					if (numShipsNeeded < (source.NumShips() / 2)) numShips = source.NumShips() / 2;
					pw.IssueOrder(source, dest, numShips);
					/* You can send more orders than just one!
					pw.IssueOrder(source, dest, 2);
					if (source.PlanetID() != dest.PlanetID() +1) pw.IssueOrder(source.PlanetID(), dest.PlanetID()+1, 2);
					*/
				}
				/*
			    if (source.NumShips() >= 50) {
			    	int numShips = source.NumShips() / 2;
			    	pw.IssueOrder(source, dest, numShips);
			    }
			    */
			}
    		
			// If enemy has no planets and is basically dead. Might be premature! :D
			if (pw.EnemyPlanets().isEmpty()) System.err.println("All your base are belong to us!");
			
    	} // isAlive()
    	else System.err.println("GG");
		
    }

    public static void main(String[] args) {
	String line = "";
	String message = "";
	int c;
	try {
	    while ((c = System.in.read()) >= 0) {
		switch (c) {
		case '\n':
		    if (line.equals("go")) {
		    	PlanetWars pw = new PlanetWars(message);
				DoTurn(pw);
				pw.FinishTurn();
				message = "";
		    } else {
			message += line + "\n";
		    }
		    line = "";
		    break;
		default:
		    line += (char)c;
		    break;
		}
	    }
	} catch (Exception e) {
	    // Owned.
	}
    }
}

