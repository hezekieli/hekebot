
public class Order {

	public int src;
	public int dest;
	public int numShips;
	
	public Order(int srcPlanet, int destPlanet, int numShipsToSend) {
		this.src = srcPlanet;
		this.dest = destPlanet;
		this.numShips = numShipsToSend;
	}

}
