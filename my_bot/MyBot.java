

import java.util.*;
import java.io.*;

/*****************************************************************
 * HekeBot2
 * Is amazing and amazingly simple! Like all the best things are.
 * 
 * It simply 
 * - sends 1 fleet per turn 
 * - from the strongest planet
 * - half the available ships
 * - if there's at least 50 ships
 * and to a planet with best score calculated according to
 * - closest distance
 * - least ships
 * - best growthrate
 * 
 * Problems ensue when there are not very high growth rate planets. 
 * Another thing to improve is accelerating the fleets sent when
 * gaining more planets.
 * 
 * @author hemijupe
 *
 */

public class MyBot {
    // The DoTurn function is where your code goes. The PlanetWars object
    // contains the state of the game, including information about all planets
    // and fleets that currently exist. Inside this function, you issue orders
    // using the pw.IssueOrder() function. For example, to send 10 ships from
    // planet 3 to planet 8, you would say pw.IssueOrder(3, 8, 10).
    //
    // There is already a basic strategy in place here. You can use it as a
    // starting point, or you can throw it out entirely and replace it with
    // your own. Check out the tutorials and articles on the contest website at
    // http://www.ai-contest.com/resources.
    public static void DoTurn(PlanetWars pw, String[] args) throws Exception {  	
		
    	// Extract the Genome
    	int[] gene = {100,100,100};			// Default values
    	//int[] gene = {100,100,100};		// Better default values
    	
    	for (int i = 0; i < 3; i++) {
    		if (i < args.length) gene[i] = Integer.parseInt(args[i]);
    	}
    	
    	
    	// Fire up the writer
    	Date timestamp = new Date();
    	PrintWriter writer = new PrintWriter("HekeBotMatchData.txt", "UTF-8");
    	writer.println(timestamp);
    	writer.close();
    	
    	// Figure out Game State
    		// GrowthRate lead
    		// Fleet lead
    		// Planet lead
    	
    	// Figure out available Fleets
    	
    	
    	// Figure out available Targets
    	
    	
    	// Sending only one fleet per planet
    	// Select some random src planets
    	int numFleetsToSend = 0;
    	if (pw.MyPlanets().size() > 5) numFleetsToSend = 5;
    	else numFleetsToSend = pw.MyPlanets().size();
    	
    	// Array of Orders
    	Order[] orders = new Order[numFleetsToSend];
    	
    	// Array of Source Planet array indexes
    	int[] fleetsToSend = new int[numFleetsToSend];
    	
    	for (int i = 0; i < numFleetsToSend; i++) {
    		
    		int rSrc = 0;
    		boolean planetAvailable = false;
    		while (!planetAvailable) {
    			rSrc = (int)(Math.random() * pw.MyPlanets().size());
    			for (int j = 0; j < numFleetsToSend; j++) {
        			if (fleetsToSend[j] != rSrc) planetAvailable = true;
        		}
    		}
    		fleetsToSend[i] = rSrc;
    		
    		// Figure out planetID of a random Target
    		int src = pw.MyPlanets().get(rSrc).PlanetID();
    		int dest = pw.NotMyPlanets().get((int)(Math.random() * pw.NotMyPlanets().size())).PlanetID();
    		int numShips = gene[0] * pw.MyPlanets().get(rSrc).NumShips();
    		// Create Order
    		Order randomOrder = new Order(src, dest, numShips);
    		orders[i] = randomOrder;
    	}
    	    	
    	// No point fighting if you don't have any planets left
    	if (pw.IsAlive(1) && !pw.MyPlanets().isEmpty()) {
    		
    		
    		
			// If enemy has no planets and is basically dead. Might be premature! :D
			if (pw.EnemyPlanets().isEmpty()) System.err.println("All your base are belong to us!");
			
    	} // isAlive()
    	else System.err.println("GG");
		
    }
    
    public static double calcPlanetScore(Planet src, Planet dest) {
    	
    	return 0;
    }

    public static void main(String[] args) {
    	String line = "";
		String message = "";
		int c;
		try {
		    while ((c = System.in.read()) >= 0) {
			switch (c) {
			case '\n':
			    if (line.equals("go")) {
			    	PlanetWars pw = new PlanetWars(message);
					DoTurn(pw, args);
					pw.FinishTurn();
					message = "";
			    } else {
				message += line + "\n";
			    }
			    line = "";
			    break;
			default:
			    line += (char)c;
			    break;
			}
		    }
		} catch (Exception e) {
		    // Owned.
		}
    }
}

