import java.util.Comparator;


public class PlanetComparator implements Comparator<Planet> {
	@Override
    public int compare(Planet p1, Planet p2) {	
        if (p1.NumShips() < p2.NumShips()) return -1;
        if (p1.NumShips() > p2.NumShips()) return 1;
        return 0;
    }
}
