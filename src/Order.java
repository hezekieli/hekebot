
public class Order {

	public int src;
	public int dest;
	public int numShips;
	public int maxShips;
	
	public Order(int srcPlanet, int destPlanet, int numShipsToSend, int maxShipsToSend) {
		this.src = srcPlanet;
		this.dest = destPlanet;
		this.numShips = numShipsToSend;
		this.maxShips = maxShipsToSend;
	}

}
