package ea;

public class MatchScore {

	public int winner;		// 1 if scored player won, 
							// -1 if lost, 
							// 0 if draw
	private int turns;		// How many turns it took the game to end
	private int turnsMax;	// How many turns was spesified as max
	private double score;			// Score of an individual game
	private double scoreMax = 200;	// Negative for loss
	private double scoreMin = 100; 	// Negative for loss
	private int turnsForMax = 50;	// If equal or less turns, gets max score.
	
	/***
	 * 
	 * @param winner_ 1 for Player 1 and the player being evolved. 2 for Player 2 and the opponent.
	 * @param turns_ How many turns did the game take.
	 * @param turnsMax What was the maximum number of turns.
	 */
	public MatchScore(int winner_, int turns_, int turnsMax_) {
		if (winner_ == 2) this.winner = -1;
		else this.winner = winner_;
		this.turns = turns_;
		this.turnsMax = turnsMax_;
		
		if (winner_ == 0) {
			if (turns_ >= turnsMax_) this.score = 0;
			else this.score = -scoreMax;
		}
		else {
			//this.score = (-this.winner) * ((scoreMax-scoreMin) / (turnsMax-turnsForMax)) * turns + this.winner * scoreMax;	// y = (-winner)(y2-y1)/(x2-x1) * x + (winner)b
			double k = ((scoreMax-scoreMin) / (turnsMax-turnsForMax));	// Slope based on maxis and mins
			double tempScore = -(k * turns) + (scoreMax + (k * 50));	// y = -(y2-y1)/(x2-x1) * x + b
			if (tempScore > scoreMax) tempScore = scoreMax;
			else if (tempScore < scoreMin) tempScore = scoreMin;
			this.score = winner * tempScore;
		}
		this.score += scoreMax;
		/*
		if (winner_ == 1) {
			this.score = -((scoreMax-scoreMin) / (turnsMax-turnsForMax)) * turns + scoreMax;	// y = -(y2-y1)/(x2-x1) * x + b
		}
		else if (winner_ == 2) {
			this.score = ((scoreMax-scoreMin) / (turnsMax-turnsForMax)) * turns - scoreMax;		// y = -(y2-y1)/(x2-x1) * x + b
		}
		else {	// if Draw
			if (turns_ >= turnsMax_) this.score = 0;
			else this.score = -300;
		}
		*/
	}
	
	public double getScore() {
		return this.score;
	}

}
