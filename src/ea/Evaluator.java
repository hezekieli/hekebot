package ea;

import java.io.*;
import java.sql.PseudoColumnUsage;
import java.util.*;
import java.util.concurrent.*;

import com.sun.jmx.snmp.Timestamp;

//import java.io.InputStream;
//import java.io.PrintWriter;
//import java.io.StringWriter;

/****
 * One own bot per evaluator.
 * 
 * 
 * @author Heke
 */
public class Evaluator implements Callable<Evaluation> {

	public String bot1Call;			// Call for the bot to evolve (genes pasted as parameters)
	public String bot2Call;			// Call string for opponent bot (includes parameters)
	public String bot1;				// Name of the bot to evolve
	public String bot2;				// Name of the opponent bot
	public int botNumber;			// To recognize from the population
	public int genes[];
	public int turnsMax;
	public int mapsToPlay;			// (ToDo) 1-100
	public int matchesToPlay;		// (ToDo)per map
	public boolean matchesMirrored;	// (ToDo)
	public int mapStart;			// starting map, 0 for random
	public boolean randomMaps;		// (ToDo) if all maps should be random
	
	public int matches = 0;			// keep track of how many have been played			
	public int winsP1 = 0;			// -''- won by player 1
	public int winsP2 = 0;			// -''- won by player 2
	public int draws = 0;
	
	public Evaluator(	String bot1Call_,
						String bot2Call_,
						int genes_[], 
						int botNumber_,
						int turnsMax_, 
						int mapsToPlay_, 
						int mapStart_, 
						boolean randomMaps_,
						int matchesToPlay_, 
						boolean matchesMirrored_) {
		
		this.genes = genes_.clone();
		this.bot1Call = bot1Call_ + " 0 " +		// 1 as first argument to tell to write HekeBotMatchData.txt -file
						genes_[0] + " " + 
						genes_[1] + " " + 
						genes_[2];
		this.bot2Call = bot2Call_;
		this.bot1 = bot1Call_.substring(0, bot1Call_.indexOf("."));
		this.bot2 = bot2Call_.substring(0, bot2Call_.indexOf("."));
		//System.out.println("bot1 = " + bot1);
		//System.out.println("bot2 = " + bot2);
		this.botNumber = botNumber_;
		
		this.turnsMax = turnsMax_;
		this.mapsToPlay = mapsToPlay_;
		this.matchesToPlay = matchesToPlay_;
		this.matchesMirrored = matchesMirrored_;
		if (mapStart_ == 0) this.mapStart = (int)((Math.random() * 100)) + 1; 
		else this.mapStart = mapStart_;
		this.randomMaps = randomMaps_;
	}

	@Override
	public Evaluation call() {
		
		// Print out my genes
		//String genesStr = "This Bots genes: [ ";
		String genesStr = "[ ";
    	for (int i = 0; i < genes.length; i++) {
    		String g;
    		if (genes[i] < 10) g = genes[i] + "  ";
    		else if (genes[i] < 100) g = genes[i] + " ";
    		else g = Double.toString(genes[i]);
    		genesStr += g + " ";
    	}
    	genesStr += "]";
    	//System.out.println(genesStr);
    	//*/

    	
		//String path = "";
		
		// Exec commands for pure cmd, Python and Bat
		/*String cmd = 	"cmd /c java -jar " + path + "PlayGame.jar " + path + "maps/map" + map + ".txt 1000 300 " + 
						path + "log.txt \"java -jar " + path + "bots/" + player1 + "\" \"java -jar " + path + 
						"bots/" + player2 + "\"";
		*/
		
		//String cmd = "cmd /c java -jar PlayGame.jar";
		
		//String cmdPy = 	"cmd.exe /c start python " + path + "pyRunMatchUp.py \"" + path + "bots/" + player1 + "\" \"" + path + "bots/" + player2 + "\" " + "\"1\" \"1\" \"0\" \"1\"";
		
		//String cmd = "cmd.exe /c start python pyRunMatchUp.py \"" + player1 + "\" \"" + player2 + "\" " + "\"10\" \"2\" \"1\" \"1\"";
		
		//String cmd =	"cmd /c start ./"+path+"pyRunMatchUp.bat";

		//System.out.println(cmd);

		

		
		Evaluation evaluation = new Evaluation(genes, botNumber);
		
		int mapNumber = this.mapStart;
		//if (this.mapStart == 0) mapNumber = (int)(Math.random() * 100) + 1;		// If random start map
		//else mapNumber = this.mapStart;
		
		PrintWriter out = null;
		PrintWriter err = null;
		
		try {
			
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
			out = new PrintWriter(new BufferedWriter(new FileWriter("EvaluatorLog.txt", false)));
			err = new PrintWriter(new BufferedWriter(new FileWriter("EvaluatorErr.txt", false)));
			out.println(new Timestamp(new Date().getTime()));
			err.println(new Timestamp(new Date().getTime()));
				
			for (int i = 0; i < this.mapsToPlay; i++) {	
				
				if (this.randomMaps) mapNumber = (int)(Math.random() * 100) + 1;
				else {
					mapNumber = this.mapStart + i;
					if (mapNumber > 100) mapNumber -= 100;
				}
				
				
				String paramBot1 = "\"java -jar bots/" + bot1Call + "\"";
				String paramBot2 = "\"java -jar bots/" + bot2Call + "\"";
				//System.out.println(paramBot1);
				//System.out.println(paramBot2);
				
				//Process process = Runtime.getRuntime().exec(cmd);
				//ProcessBuilder pb = new ProcessBuilder("cmd", "/C", "java", "-jar", "PlayGame.jar");
				ProcessBuilder pb = new ProcessBuilder("java", "-jar", "PlayGame.jar", "maps/map" + mapNumber + ".txt", "1000", Integer.toString(this.turnsMax), "log.txt", paramBot1, paramBot2);
				pb.redirectErrorStream(true);
				Process process = pb.start();
				
				//System.out.println(pb.command().toString());
				
	
				
				// Get input streams
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				
				String line = null;
				
				int result = -1;
				int turns = 0;
				String resultStr = "";
				String timeout = "";
				
				// Read command standard output       
				//System.out.println("Standard output (InputStream): ");
	            while ((line = stdInput.readLine()) != null) {
	            	
	            	
	            	//System.out.println("line: " + line);
	                //out.println(line);
	            	if (line.contains("timed out")) {
	            		timeout = "Timed out";
	            	}
	                if (line.contains("Turn ")) {
						turns++;
						//System.out.println("Turn " + turns);
					}
	                if(line.contains("Player 1 Wins!")) {
						//System.out.println("["+botNumber+"] " + genesStr + " Match " + (i+1) + "/" + this.mapsToPlay + "\t Map " + mapNumber + ": \t " + bot1 + " won! (" + turns + ") " + timeout);
						resultStr = bot1 + " won!";
						winsP1++;
						result = 1;
						break;
					}
					if(line.contains("Player 2 Wins!"))
					{
						//System.out.println("["+botNumber+"] " + genesStr + " Match " + (i+1) + "/" + this.mapsToPlay + "\t Map " + mapNumber + ": \t " + bot2 + " won! (" + turns + ") " + timeout);
						resultStr = bot2 + " won!";
						winsP2++;
						result = 2;
						break;
					}
					if(line.contains("Draw"))
					{
						//System.out.println("["+botNumber+"] " + genesStr + " Match " + (i+1) + "/" + this.mapsToPlay + "\t Map " + mapNumber + ": \t " + "Draw! (" + turns + ") " + timeout);
						resultStr = "Draw...\t";
						result = 0;
						break;
					}
	            }
	            
	            MatchScore ms = new MatchScore(result, turns, this.turnsMax);
	            evaluation.matchScores.add(ms);
	            
	            String toPrint = "["+botNumber+"] Match " + (i+1) + "/" + this.mapsToPlay + "\t Map " + mapNumber + ": \t " + resultStr + "\t Turns: " + turns + " " + timeout + "\t Score: " + ms.getScore();
	            //System.out.println(toPrint);
	            writer.println(toPrint);
				
				
				/* Read command errors
				System.out.println("Standard output (ErrorStream): ");
				
				while ((line = stdError.readLine()) != null) {
					//err.println(line);
					//System.out.println(line);
				}
				
				*/
	
				//System.out.println("Closing the streams, destroying the process and closing the files...");
				//stdInput.close();
				stdError.close();
				process.destroy();
				
				out.close();
				err.close();
				//System.out.println("All closed and destroyed.");
				
			}	// for loop for Maps
			
			writer.close();
			
		}
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace(System.err);
		} finally {
			//out.close();
			err.close();
		}
		
		//evaluation.randomScore();
		evaluation.calculateScore();
		return evaluation;
			
			
		
		

	}

}
