package ea;

public class Genome {

	public int genes[];
	public int botNumber = 0;			// To recognize from the population
	
	public Genome(int g[], int botNumber_) {
		this.genes = g.clone();
		this.botNumber = botNumber_;
	}
	
	public Genome(int g[]) {
		this.genes = g.clone();
	}
	
	@Override
	public String toString() {
		String s = "";
		if (botNumber < 10) s = "["+botNumber+"   ][";
		else if (botNumber < 100) s = "["+botNumber+"  ][";
		else if (botNumber < 1000) s = "["+botNumber+" ][";
		else s = "["+botNumber+"][";
    	for (int i = 0; i < genes.length; i++) {
    		String g;
    		if (genes[i] < 10) g = genes[i] + "  ";
    		else if (genes[i] < 100) g = genes[i] + " ";
    		else g = genes[i] + "";
    		s += g + " ";
    	}
    	s += "]";
		return s;
	}

}
