package ea;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.sun.jmx.snmp.Timestamp;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import com.sun.org.apache.bcel.internal.generic.NEW;
//import org.apache.commons.io.*;


public class Evolution {
	
	/*
	public Evaluation call() throws Exception {
		Evaluation e = new Evaluation();  
		return e;
	}
	*/
	
	public static int genesNum = 19;
	public static int geneMin = 0;
	public static int geneMax = 100;
	public static int crossovers = 0;
	public static int mutations = 0;
	
	public static void main(String[] args) throws Exception {

		/****************************************************************
		 * GENOME affects the interpreting, weighing and decision making
		 ****************************************************************/
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
		writer.println(new Date());
		writer.close();
		
		// Evaluation variables
		String bot = "HekeBot.jar";			// Name of bot to evolve
		//String opponent = "HekeBot7.jar";	// Name of opponent file and parameters
		String opponent = "Oskari_ReactiveBot.jar 3.2714075 -1.2531304 -3.0435133 4.0648925 -4.4002074 3.252921 0.871469 0.754116 3.8006549 0.95189536";
		//String opponent = "Oskari_ReactiveBot.jar -3.6035961 -0.9712982 4.230381 0.272633 -4.40119207 3.627777 3.0098474 3.353147 4.9688564 0.2034666062";
		int turnsMax = 200;
		int mapsToPlay = 5;				// 1-100
		int matchesToPlay = 1;				// per map
		boolean matchesMirrored = false;
		int mapStart = 0;					// starting map, 0 for random
		boolean randomMaps = true;
		
		// Population variables
		int botNum = 0;
		int popSize = 10;
		int generations = 10;
		double crossoverProb = 0.9;
		double mutationProb = 0.01;
		
		
		PrintWriter hekeBot = null;
		hekeBot = new PrintWriter(new BufferedWriter(new FileWriter("HekeBotMatchData.txt", false)));
		Date timestamp = new Date();
		hekeBot.println(timestamp);
		hekeBot.close();
		
		writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
		
		
		List<Genome> population = new ArrayList<Genome>();
		
		for (int i = 0; i < popSize; i++) {
			Random rand = new Random();
			int g[] = new int[genesNum];
			for (int j = 0; j < genesNum; j++) {
				g[j] = rand.nextInt(geneMax+1);
			}
    		//int g [] = {rand.nextInt(100), rand.nextInt(100), rand.nextInt(100)};
			Genome genome = new Genome(g, botNum);
    		population.add(genome);
    		botNum++;
    		System.out.println(genome.toString());
		}

		
		/*
		int score = 0;
		System.out.println("Testaan scoren laskukaavaa.");
		score = -(1) * ((300-50) / (300-50)) * 50 + (1) * 300;
		System.out.println(score);
		score = -(-1) * ((300-50) / (300-50)) * 50 + (-1) * 300;
		System.out.println(score);
		score = -(1) * ((300-50) / (300-50)) * 300 + (1) * 300;
		System.out.println(score);
		score = -(-1) * ((300-50) / (300-50)) * 300 + (-1) * 300;
		System.out.println(score);
		score = -(1) * ((300-50) / (300-50)) * 150 + (1) * 300;
		System.out.println(score);
		score = -(-1) * ((300-50) / (300-50)) * 200 + (-1) * 300;
		System.out.println(score);
		*/
		
		// LOOP-start Put them to fight
		
		/* for loop for different number of threads
		for (int t = 1; t < 5; t++) {
			
			writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
			System.out.println("\nTHREADS: " + t + ":");
			writer.println("");
			writer.println("THREADS: " + t + ":");
			writer.println(new Date());
			writer.close();
			*/
			// This runs the bots separately
			ExecutorService exSer = Executors.newFixedThreadPool(2);
			
			for (int gen = 0; gen < generations; gen++) {
				
				System.out.println("\nGENERATION " + gen + ":");
				writer.println("");
				writer.println("GENERATION " + gen + ":");
				writer.close();
				
				// List to store the Evaluations
				List<Future<Evaluation>> futures = new ArrayList<Future<Evaluation>>();
				List<Evaluation> evaluations = new ArrayList<Evaluation>(); 
		
				
				// Here we start all the Evaluators ie. bots
		        //for(int i=0; i< 5; i++){
				if (!population.isEmpty()) {
					
					for (Genome genome : population) { 
						
			        	// We create callable evaluators with different gene parameters
			        	Callable<Evaluation> hekebot = new Evaluator(bot, opponent, genome.genes, genome.botNumber, turnsMax, mapsToPlay, mapStart, randomMaps, matchesToPlay, matchesMirrored);
			        	
			            //submit Callable tasks to be executed by thread pool
			            Future<Evaluation> future = exSer.submit(hekebot);
			            
			            //add Future to the list, we can get return value using Future
			            futures.add(future);
			        }
				}
		        
				// Record
				
				// And here we wait for the results
				if (!futures.isEmpty()) {
		        	for (Future<Evaluation> fut : futures){
			            try {
			                //print the return value of Future, notice the output delay in console
			                // because Future.get() waits for task to get completed
			            	Evaluation eva = fut.get();
			            	evaluations.add(eva);
			            	System.out.println("Results from ["+eva.botNumber+"]...");
			            	//future = fut;
				
			            } catch (InterruptedException | ExecutionException e) {
			                e.printStackTrace();
			            } 
		        	}
				}
		        	//futures.remove(future);
		        //}
		        
				
				// Select
				Collections.sort(evaluations, new EvaluationComparator());
				
				// For the basis of Selection probability
				double totalScores = 0;
				
				writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
		        System.out.println("Evaluations of Generation " + gen + ":");
		        writer.println("Evaluations of Generation " + gen + ":");
		        for (Evaluation eva : evaluations) {
		        	
		        	totalScores += eva.getScore();
		        	
		        	String toPrint = "[ ";
		        	for (int i = 0; i < eva.genes.length; i++) {
		        		String g;
		        		if (eva.genes[i] < 10) g = eva.genes[i] + "  ";
		        		else if (eva.genes[i] < 100) g = eva.genes[i] + " ";
		        		else g = Double.toString(eva.genes[i]);
		        		toPrint += g + " ";
		        	}
		        	toPrint += "]";
		        	
		        	System.out.println("["+eva.botNumber+"] " + toPrint + " :: " + eva.getScore());
		        	writer.println("["+eva.botNumber+"] " + toPrint + " :: " + eva.getScore());
		            //System.out.println(new Date()+ " :: HekeBot "+ toPrint + " :: " + eva.getScore());
		        }
		        
		        System.out.println("");
		        
		        // New Population
		        population = new ArrayList<Genome>();
				Genome alpha = new Genome(evaluations.get(0).genes, evaluations.get(0).botNumber);
		        population.add(alpha);
		        
		        // Crossover them
		        	// Loop for population size - alpha(s)
		        	// Select two parents for each offspring
		        	// Make sure they are not both the same
		        	// Do crossover with their genes at random point
		        	// Compose a new genome
		        for (int i = 0; i < popSize-1; i++) {
		        	int p1 = selectParent(evaluations, totalScores);
		        	int p2 = selectParent(evaluations, totalScores);
		        	while (p1 == p2) {
		        		p2 = selectParent(evaluations, totalScores);
		        	}
			        Genome parent1 = new Genome(evaluations.get(p1).genes, evaluations.get(p1).botNumber);
			        Genome parent2 = new Genome(evaluations.get(p2).genes, evaluations.get(p2).botNumber);
			        
			        Genome offspring = crossOver(parent1.genes, parent2.genes, crossoverProb);
			        offspring.botNumber = botNum;
			        
			        System.out.println("Parent1["+evaluations.get(p1).botNumber+"]\t = " + parent1.toString());
			        System.out.println("Parent2["+evaluations.get(p2).botNumber+"]\t = " + parent2.toString());
			        System.out.println("Offspring\t = " + offspring.toString());
			        
			        Genome mutated = injectMutation(offspring.genes, mutationProb);
			        mutated.botNumber = botNum;
			        
			        System.out.println("Mutated \t = " + mutated.toString() + "\n");
			        
			        botNum++;
			        
			        population.add(mutated);
		        }
		        
		        System.out.println("Crossovers = " + crossovers + "/"+popSize);
		        System.out.println("Mutations = " + mutations + "/"+(popSize*genesNum) + "\n");
		        
		        for (int i = 0; i < population.size(); i++) {
		        	System.out.println(population.get(i).toString());
		        }
		        
				
		        // Inject mutations
		        
	        	// Replace population
		        
		        writer.close();
		        
		        // LOOP-end (Put the new population to fight)
		        
			}	// End of Generation
			
			//shut down the executor service now
	        exSer.shutdown();
			
	        writer = new PrintWriter(new BufferedWriter(new FileWriter("EvolutionLog.txt", true)));
			writer.println(new Date());
			writer.println("");
			writer.close();
			
			
		//}	// for loop for different number of threads
		
		
	}
	
	public static int selectParent(List<Evaluation> evas, double totalScores) {
		int selected = 0;									// index of the selected on the list
		double selector = evas.get(0).getScore();			// This goes looking for the selected parent
		double marker = (Math.random() * totalScores);		// Randomizer
		//System.out.println("Marker = " + marker + ", TotalScores = " + totalScores);
		//System.out.println("["+selected+"] Selector at " + selector);
		while (marker >= selector && selected < evas.size()) {
			selected++;
			selector += evas.get(selected).getScore();
			//System.out.println("["+selected+"] Selector at " + selector);
		}
		//System.out.println("["+selected+"] Selector at " + selector);
		return selected;
	}
	
	/**
	 * Crossover checks that both genomes are of same length. It is build in that 
	 * @param g1
	 * @param g2
	 * @param coProbability 	Value between 0 and 1 for probability of crossover happening. 
	 * 							In case crossover doesn't happen, the parent1 is returned.
	 * @return
	 */
	public static Genome crossOver(int g1[], int g2[], double coProbability) {
		if (g1.length == g2.length && coProbability >= 0 && coProbability <= 1) {
			if (Math.random() < coProbability) {
				int g[] = new int[g1.length];
				int coIndex = (int)(1 + Math.random() * (g1.length -1));
				//System.out.println("crossOverIndex = " + coIndex);
				crossovers++;
				for (int i = 0; i < g1.length; i++) {
					if (i < coIndex) g[i] = g1[i];
					else g[i]= g2[i];
				}
				Genome offspring = new Genome(g);
				return offspring;
			}
			else return new Genome(g1);
		}
		else return new Genome(g1);
	}
	
	/**
	 * 
	 * @param g
	 * @param mutProbability	Value between 0 and 1 for probability of mutation happening for each gene.
	 * @return
	 */
	public static Genome injectMutation(int g[], double mutProbability) {
		if (mutProbability >= 0 && mutProbability <= 1) {
			int l = g.length;
			for (int i = 0; i < l; i++) {
				if (Math.random() < mutProbability) {
					g[i] = (int)(Math.random() * (geneMax + 1));
					//System.out.println("MUTATION!");
					mutations++;
				}
			}
			return new Genome(g);
		}
		else return new Genome(g);
	}

}
