package ea;

import java.util.Comparator;

public class EvaluationComparator implements Comparator<Evaluation> {

	public EvaluationComparator() {
		// TODO Auto-generated constructor stub
	}
	
	public int compare(Evaluation e1, Evaluation e2) {
		if (e1.getScore() < e2.getScore()) return 1;
        else if (e1.getScore() > e2.getScore()) return -1;
        else  return 0;
	}

}
