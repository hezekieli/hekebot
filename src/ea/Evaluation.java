package ea;

import java.util.ArrayList;
import java.util.List;

public class Evaluation {

	public int genes[];
	public int botNumber;
	private double score;
	public List<MatchScore> matchScores;
	public double breedProbability;			// According to scores of the first and last
	//public boolean alpha;					// Elitism, the alpha survives to the next generations battles
	
	/*************************
	 * Here is only the necessary information to evaluate how fit the bot was.
	 * Nothing about running the bot is needed here. 
	 * This is just a smart dataObject. :)
	 * 
	 * How many wins vs loses (win percent?)
	 * How many turns on avg when won?
	 * How many turns on avg when lost?
	 */
	public Evaluation(int g[], int botNumber_) {
		this.genes = g.clone();
		this.botNumber = botNumber_;
		this.matchScores = new ArrayList<MatchScore>();
	}
	
	public double getScore() {
		return this.score;
	}
	
	public void setScore(double s) {
		this.score = s;
	}
	
	public double calculateScore() {
		if (!this.matchScores.isEmpty()) {
			double scoreTotal = 0;
			for (MatchScore ms : this.matchScores) {
				scoreTotal += ms.getScore();
			}
			this.score = scoreTotal / matchScores.size();
		}
		return this.score;
	}
	
	public void randomScore() {
		this.score = (int)(Math.random() * 101);
	}

}
