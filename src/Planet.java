

public class Planet implements Cloneable {
	
	private int planetID;
    private int owner;
    private int numShips;
    private int numShipsLeft;
    private int growthRate;
    private double score;
    private double scoreTotal;
    private int dist;				// Temporary distance calculated for the current Source Planet
    public double distC;			// Temporary comparable distance value
    public int payoff;				// Temporary payoff calculated for the current Source Planet
    private double x, y;
    private boolean isBase;
    private boolean isTarget;
    private int shipsBalance;		// Reinforcements - attacks (- defences). If > 0, all fine
    private int shipsAttacking;		// Number of ships attacking this planet. If = 0, peaceful
    private int shipsReinforcing;	// Number of reinforcing ships incoming
    //private int closestIncoming;	// How far is the nearest incoming attacking fleet
    private int[] fleets;			// How many ships arriving at each turn (+ own ships - enemy ships)
    private int[] needForReinf;		// index is the distance from which planning to send
    								// content is number of ships needed by the time reinforcements reach the planet
    //private int status;			// 0 = Peaceful, 1 = attack, 2 = heavy attack
	
    // Initializes a planet.
    public Planet(int planetID,
                  int owner,
				  int numShips,
				  int growthRate,
				  double x,
				  double y) {
		this.planetID = planetID;
		this.owner = owner;
		this.numShips = numShips;
		this.numShipsLeft = numShips;
		this.growthRate = growthRate;
		double o = 0.3;
		if (this.owner == 2) o = 0.6;
		this.score = ((50*growthRate) + (10*growthRate*growthRate)) + (o * (500-numShips));
		this.scoreTotal = 0;
		this.dist = 100;
		this.payoff = 0;
		this.x = x;
		this.y = y;
		this.isBase = false;
		this.isTarget = false;
		this.fleets = new int[30];
		this.needForReinf = new int[30];
		if (owner == 1) needForReinf[0] = numShips;
		else needForReinf[0] = -numShips;
    }

    // Accessors and simple modification functions. These should be mostly
    // self-explanatory.
    public int PlanetID() {
	return planetID;
    }

    public int Owner() {
	return owner;
    }

    public int NumShips() {
	return numShips;
    }
    
    /*****************************************************
     * Enables sending more than one fleet from each planet. 
     * @param subtraction Number of ships to subtract from Planets reserves on this Turn.
     * @return True if there are enough ships left to subtract from. False if not enough.
     */
    public boolean SubtractNumShipsLeft(int subtraction) {
    	if (subtraction <= this.numShips && subtraction <= this.numShipsLeft) {
    		this.numShipsLeft -= subtraction;
    		return true;
    	}
    	else return false;
    }
    
    public int NumShipsLeft() {
    return numShipsLeft;
    }

    public int GrowthRate() {
	return growthRate;
    }
    
    public double Score() {
	return score;
    }
    
    public void SetDist(int d) {
    	this.dist = d;
    }
    
    public int Dist() {
    	return dist;
    }
    
    public void setScoreTotal(double st) {
    	this.scoreTotal = st;
    }
    
    public double ScoreTotal() {
    	return scoreTotal;
    }

    public double X() {
	return x;
    }

    public double Y() {
	return y;
    }
    
    public void setIsBase(boolean b) {
    	isBase = b;
    }
    
    public boolean IsBase() {
    	return isBase;
    }
    
    public void setIsTarget(boolean b) {
    	isTarget = b;
    }
    
    public boolean IsTarget() {
    	return isTarget;
    }

    public void Owner(int newOwner) {
	this.owner = newOwner;
    }

    public void NumShips(int newNumShips) {
	this.numShips = newNumShips;
    }

    public void AddShips(int amount) {
	numShips += amount;
    }

    public void RemoveShips(int amount) {
	numShips -= amount;
    }
    
    public int ShipsBalance() {
    	return this.shipsBalance;
    }
    
    public void SetShipsBalance(int amount) {
    	this.shipsBalance = amount;
    }
    
    public void AddShipsBalance(int amount) {
    	this.shipsBalance += amount;
    }
    
    public int ShipsAttacking() {
    	return this.shipsAttacking;
    }
    
    public void SetShipsAttacking(int amount) {
    	this.shipsAttacking = amount;
    }
    
    public void AddShipsAttacking(int amount) {
    	this.shipsAttacking += amount;
    }
    
    public int ShipsReinforcing() {
    	return this.shipsReinforcing;
    }
    
    public void SetShipsReinforcing(int amount) {
    	this.shipsReinforcing = amount;
    }
    
    public void AddShipsReinforcing(int amount) {
    	this.shipsReinforcing += amount;
    }
 
    public boolean AddFleet(int turn_, int numShips_) {
    	if (turn_ < 30) {
    		this.fleets[turn_] += numShips_;
    		return true;
    	}
    	else return false;
    }
    

    private Planet (Planet _p) {
	planetID = _p.planetID;
	owner = _p.owner;
	numShips = _p.numShips;
	growthRate = _p.growthRate;
	x = _p.x;
	y = _p.y;
    }

    public Object clone() {
	return new Planet(this);
    }
}
