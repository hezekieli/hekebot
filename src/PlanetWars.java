

// Contestants do not need to worry about anything in this file. This is just
// helper code that does the boring stuff for you, so you can focus on the
// interesting stuff. That being said, you're welcome to change anything in
// this file if you know what you're doing.

import java.util.*;
import java.io.*;

//import ea.Genome;

public class PlanetWars {
	
	// Default Genes
	private double genes[] = {	50,		// 0 = double scoreWeightGr		= [0 , 100]	(0 - 5)
								50, 	// 1 = double scoreWeightDist		= [0 , 20]	(1 - ~30)
								50,		// 2 = double scoreWeightShips	= [0 , 1]	(0 - 300...1000?)
								1,		// 3 = int numOfOrdersMax									= [1 , 10]	
								30,		// 4 = int numOfOrdersMax (isBase / overwhelm)			= [1 , 20]
								50,		// 5 = int numOfPlanetsHandledMax							= [1 , 10]
								50,		// 6 = int numOfPlanetsHandledMax (isBase / overwhelm)	= [1 , 20]
								30,		// 7 = double shipsMin			= [0 , 1]
								70,		// 8 = double shipsMax			= [0 , 1]	(After (1 - shipsMin) or (1 - shipsRequired))
								40,		// 9 = double overwGrLeadMin		= [0 , 1]
								40,		// 10 = double overwShipLeadMin	= [0 , 1]
								20,		// 11 = double grLead				= [0 , 1]	When player considered in lead (Used as negative also)
								10,		// 12 = double shipLead			= [0 , 1]	When player considered in lead (Used as negative also)
								60,		// 13 = double baseWeightOwn		= [0 , 1]
								20,		// 14 = double baseWeightEnemy		= [0 , 1]
								20,		// 15 = double baseWeightAll		= [0 , 1]
								60,		// 16 = double targetWeightOwn		= [0 , 1]
								20,		// 17 = double targetWeightEnemy	= [-1 , 1]		Should we attack close to or far from Enemy Center?
								50		// 18 = double targetWeightGr		= [0 , 100]
								};
	
	// Store all the planets and fleets. OMG we wouldn't wanna lose all the
    // planets and fleets, would we!?
    private ArrayList<Planet> planets;
    private ArrayList<Fleet> fleets;
    
    public int distances[][];	// Store calculated distances for optimization
    public double centOfGAll[] = new double[2];
    public double centOfGP1[] = new double[2];
    public double centOfGP2[] = new double[2];    		
	

	// Distance between starting planets
    public int distanceStart;
	// "Max" left-right distance between planets
    public int distanceMax;
	
	
	// Player Total GrowthRates
    public int grP0 = 0;
    public int grP1 = 0;
    public int grP2 = 0;
    public int grLead;
    public int grTotal;
    public double grLeadPerc;
	
	// Player Total Ships
    public int shipsP0 = 0;
    public int shipsP1 = 0;
    public int shipsP2 = 0;
    public int shipsLead;
    public int shipsTotal;
    public double shipLeadPerc;
    
    // Constructs a PlanetWars object instance, given a string containing a
    // description of a game state.
    public PlanetWars(String gameStateString, String args[]) {
		
    	long startTime = System.nanoTime();
    	long elapsedTime = (System.nanoTime() - startTime) / 1000000;
    	//System.err.println(elapsedTime + " StartTime~");

    	
    	planets = new ArrayList<Planet>();
		fleets = new ArrayList<Fleet>();
		ParseGameState(gameStateString);
		
		// Remove the first parameter to make extracting genome simpler
		String temp[] = new String[args.length-1];
		String toPrint = "This bots args("+args.length+"): [ ";
		for (int i = 1; i < args.length; i++) {
			toPrint += args[i]+ " ";
			temp[i-1] = args[i];
		}
		toPrint += "] \n";
		System.err.println(toPrint);
		SetGenes(temp);
		
		// Own Code, MACRO
		distances = new int[planets.size()][planets.size()];	// Store calculated distances for optimization
		// Planet center of "Gravities"
		// Based on coordinates and the weights (GrowthRates) of the planets
		
		if (IsAlive(1) && !MyPlanets().isEmpty()) {
					
			//writer.println("Figuring out all the planets...");
			//elapsedTime = (System.nanoTime() - startTime) / 1000000;
			//System.err.println(elapsedTime + " Figuring out all the planets...");
			// Loop through the planets to Extract data
			//List<Planet> planetsMacro = new ArrayList<Planet>(planets);
			//Collections.sort(planetsMacro, new PlanetComparator(1));
			
			int farthestLeft = 0;
		    int farthestRight = 0;
			
			for (Planet p : planets) {
				
				if (p.X() <= GetPlanet(farthestLeft).X()) farthestLeft = p.PlanetID();
				if (p.X() >= GetPlanet(farthestRight).X()) farthestRight = p.PlanetID();
				
				switch (p.Owner()) {
				case 0: 
					centOfGAll[0] += p.GrowthRate() * p.X();
					centOfGAll[1] += p.GrowthRate() * p.Y();
					grP0 		+= p.GrowthRate();
					shipsP0 	+= p.NumShips();
					p.SetShipsBalance(-p.NumShips());
					break;
				case 1: 
					centOfGAll[0] += p.GrowthRate() * p.X();
					centOfGAll[1] += p.GrowthRate() * p.Y();
					centOfGP1[0] += p.GrowthRate() * p.X();
					centOfGP1[1] += p.GrowthRate() * p.Y();
					grP1 		+= p.GrowthRate();
					shipsP1 	+= p.NumShips();
					p.SetShipsBalance(p.NumShips());
					break;
				case 2: 
					centOfGAll[0] += p.GrowthRate() * p.X();
					centOfGAll[1] += p.GrowthRate() * p.Y();
					centOfGP2[0] += p.GrowthRate() * p.X();
					centOfGP2[1] += p.GrowthRate() * p.Y();
					grP2 		+= p.GrowthRate();
					shipsP2 	+= p.NumShips();
					p.SetShipsBalance(-p.NumShips());
					break;
				default: break;
				}
			}
			distanceMax = Distance(GetPlanet(farthestLeft), GetPlanet(farthestRight));
			distanceStart = Distance(1, 2);
			
			// writer.println("Figuring out all the fleets...");
			
			// Loop through the fleets to Extract data
			for (Fleet f : fleets) {
				int numships = f.NumShips();
				int destID = f.DestinationPlanet();
				Planet destP = GetPlanet(destID);  
				switch (f.Owner()) {
				case 0: 						// Hopefully no fleets from the neutral player...
					shipsP0 	+= numships;
					break;
				case 1: 
					shipsP1 	+= numships;
					destP.AddShipsBalance(numships);
					destP.AddShipsReinforcing(numships);
					destP.AddFleet(f.TurnsRemaining(), numships);
					break;
				case 2: 
					shipsP2 	+= numships;
					//int subtractships = -numships;
					destP.AddShipsBalance(-numships);
					destP.AddShipsAttacking(numships);
					destP.AddFleet(f.TurnsRemaining(), -numships);
					break;
				default: break;
				}
			}
			
			// Positive if player 1 has more ships
			shipsLead = shipsP1 - shipsP2;
			shipsTotal = shipsP0 + shipsP1 + shipsP2;
			shipLeadPerc = 0.01 * (int)(100*((double)shipsLead)/((double)shipsTotal-shipsP0));
			
			// Positive if player 1 has bigger growth rate
			grLead = grP1 - grP2;
			grTotal = grP0 + grP1 + grP2;
			
			int grP1and2 = grTotal-grP0;
			if (grP1and2 > 0) {
				grLeadPerc = 0.01 * (int)(100*((double)grLead)/((double)grP1and2));
			} else grLeadPerc = 0;
			
			//elapsedTime = (System.nanoTime() - startTime) / 1000000;
			System.err.println(elapsedTime + " Calculating centers of GR...");
			
			
			centOfGAll[0] = 0.01 * (int)(100*centOfGAll[0]/grTotal);
			centOfGAll[1] = 0.01 * (int)(100*centOfGAll[1]/grTotal);
			
			if (grP1 > 0) {
				centOfGP1[0] = 0.01 * (int)(100*centOfGP1[0]/grP1);
				centOfGP1[1] = 0.01 * (int)(100*centOfGP1[1]/grP1);
			}
			else {
				centOfGP1[0] = GetPlanet(0).X();
				centOfGP1[1] = GetPlanet(0).Y();
			}
			if (grP2 > 0) {
				centOfGP2[0] = 0.01 * (int)(100*centOfGP2[0]/grP2);
				centOfGP2[1] = 0.01 * (int)(100*centOfGP2[1]/grP2);
			}
			else {
				centOfGP2[0] = GetPlanet(0).X();
				centOfGP2[1] = GetPlanet(0).Y();
			}
			
			//System.err.println("All["+centOfGAll[0]+"]["+centOfGAll[1]+"]");
			//System.err.println("P1["+centOfGP1[0]+"]["+centOfGP1[1]+"]");
			//System.err.println("P2["+centOfGP2[0]+"]["+centOfGP2[1]+"]");

			
			//elapsedTime = (System.nanoTime() - startTime) / 1000000;
			//System.err.println(elapsedTime + " Target planet decided");
		}
    }
    
    private void SetGenes(String temp[]) {
    	// To check if there's more args or genes to paste
    	//int maxI = ((args.length -1) >= genes.length) ? genes.length : args.length;
    	int max = genes.length;
    	double gene = 0;
    	double g = 0;
    	for (int i = 0; i < max; i++) {
    		if (i < temp.length) {		
        		gene = Integer.parseInt(temp[i]);
    		}
    		else gene = genes[i];
    		
			if (gene >= 0 && gene <=  100) {	// To check that the genes value is in valid range
				switch (i) {
					case 0:		// double scoreWeightGr			= [0 , 100]	(0 - 5)
						genes[i] = gene;
						break;
					
					case 1: 	// double scoreWeightDist		= [0 , 20]	(1 - ~30)
						genes[i] = gene / 5.0;
						break;
						
					case 2:		// double scoreWeightShips		= [0 , 1]	(0 - 300...1000?)
						genes[i] = gene / 100.0;
						break;
						
					case 3:		// int numOfOrdersMax								= [1 , 10]	
						g = (int)(gene / 10.0);
						if (g == 0) g = 1;
						genes[i] = g;
						break;
						
					case 4: 	// int numOfOrdersMax (isBase / overwhelm)			= [1 , 20]
						g = (int)(gene / 5.0);
						if (g == 0) g = 1;
						genes[i] = g;
						break;
						
					case 5:		// int numOfPlanetsHandledMax						= [1 , 10]
						g = (int)(gene / 10.0);
						if (g == 0) g = 1;
						genes[i] = g;
						break;
						
					case 6:		// int numOfPlanetsHandledMax (isBase / overwhelm)	= [1 , 20]
						g = (int)(gene / 5.0);
						if (g == 0) g = 1;
						genes[i] = g;
						break;
						
					case 7:		// double shipsMin		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 8:		// double shipsMax		= [0 , 1]	([shipsMin , 1])
						g = 1 - genes[i-1];
						genes[i] = genes[i-1] + ((g * gene) / 100.0);
						break;
						
					case 9:		// double overwGrLeadMin		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 10:	// double overwShipLeadMin		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 11:	// double grLead				= [0 , 1]	When player considered in lead (Used as negative also)
						genes[i] = gene / 100.0;
						break;
						
					case 12:	// double shipLead				= [0 , 1]	When player considered in lead (Used as negative also)
						genes[i] = gene / 100.0;
						break;
							
					case 13: // double baseWeightOwn		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 14: // double baseWeightEnemy		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 15: // double baseWeightAll		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 16: // double targetWeightOwn		= [0 , 1]
						genes[i] = gene / 100.0;
						break;
						
					case 17: // double targetWeightEnemy	= [-1 , 1]		Should we attack close to or far from Enemy Center?
						genes[i] = (gene / 50.0) -1;
						break;
						
					case 18: // double targetWeightGr		= [0 , 100]
						genes[i] = gene;
						break;
						
					default:
						break;
				}
			}
		}
    	
    	String toPrint = "This bots genes: [ ";
    	
    	for (int i = 0; i < genes.length; i++) {
    		toPrint += genes[i]+ " ";
    	}
    	toPrint += "]";
    	System.err.println(toPrint);
    }
    
    /** GENES:
	 * 
	 * MICRO
	 * Weighing Target Score
	 * 	0 = double scoreWeightGr		= [0 , 100]	(0 - 5)
	 * 	1 = double scoreWeightDist		= [0 , 20]	(1 - ~30)
	 * 	2 = double scoreWeightShips		= [0 , 1]	(0 - 300...1000?)
	 * 
	 * Number of orders
	 * 	3 = int numOfOrdersMax								= [1 , 10]	
	 * 	4 = int numOfOrdersMax (isBase / overwhelm)			= [1 , 20]
	 * 	5 = int numOfPlanetsHandledMax						= [1 , 10]
	 * 	6 = int numOfPlanetsHandledMax (isBase / overwhelm)	= [1 , 20]
	 * 
	 * How number of ships to send is decided
	 * 	7 = double shipsMin		= [0 , 1]
	 * 	8 = double shipsMax		= [0 , 1]	(After (1 - shipsMin) or (1 - shipsRequired))
	 * 
	 * MACRO
	 * How StateMacro is defined
	 * 	9 = double overwGrLeadMin		= [0 , 1]
	 * 	10 = double overwShipLeadMin		= [0 , 1]
	 * 	11 = double grLead				= [0 , 1]	When player considered in lead (Used as negative also)
	 * 	12 = double shipLead				= [0 , 1]	When player considered in lead (Used as negative also)
	 * 
	 * 	double attackGrLeadMax		= [-1 , 0]
	 * 	double attackShipLeadMin	= [0 , 1]
	 * 	double defendGrLeadMin		= [0 , 1]
	 * 	double defendShipLeadMax	= [-1 , 0]
	 * 
	 * How Base and Target planets is decided based on Centers of GR
	 * 	13 = double baseWeightOwn		= [0 , 1]
	 * 	14 = double baseWeightEnemy		= [0 , 1]
	 * 	15 = double baseWeightAll		= [0 , 1]
	 * 
	 * 	16 = double targetWeightOwn		= [0 , 1]
	 * 	17 = double targetWeightEnemy	= [-1 , 1]		Should we attack close to or far from Enemy Center?
	 * 	18 = double targetWeightGr		= [0 , 100]
	 * 
	 * How StateGame is defined (If it's even used...)
	 * 	double lateGameNeutralMax	= [0 , 1]
	 * 	double midGameNeutralMax	= [0 , 1]
	 * 
	 * Harass probability
	 * 	double harassProb	= [0 , 0.1]	(1->0.001, 100->0.1)
	 */
    public double Gene(int index) {
    	if (index < genes.length) return genes[index];
    	else return 0;
    }
    
    public int GeneInt(int index) {
    	return (int)Gene(index);
    }

    // Returns the number of planets. Planets are numbered starting with 0.
    public int NumPlanets() {
	return planets.size();
    }

    // Returns the planet with the given planet_id. There are NumPlanets()
    // planets. They are numbered starting at 0.
    public Planet GetPlanet(int planetID) {
	return planets.get(planetID);
    }

    // Returns the number of fleets.
    public int NumFleets() {
	return fleets.size();
    }

    // Returns the fleet with the given fleet_id. Fleets are numbered starting
    // with 0. There are NumFleets() fleets. fleet_id's are not consistent from
    // one turn to the next.
    public Fleet GetFleet(int fleetID) {
	return fleets.get(fleetID);
    }

    // Returns a list of all the planets.
    public List<Planet> Planets() {
	return planets;
    }

    // Return a list of all the planets owned by the current player. By
    // convention, the current player is always player number 1.
    public List<Planet> MyPlanets() {
	List<Planet> r = new ArrayList<Planet>();
	for (Planet p : planets) {
	    if (p.Owner() == 1) {
		r.add(p);
	    }
	}
	return r;
    }

    // Return a list of all neutral planets.
    public List<Planet> NeutralPlanets() {
	List<Planet> r = new ArrayList<Planet>();
	for (Planet p : planets) {
	    if (p.Owner() == 0) {
		r.add(p);
	    }
	}
	return r;
    }

    // Return a list of all the planets owned by rival players. This excludes
    // planets owned by the current player, as well as neutral planets.
    public List<Planet> EnemyPlanets() {
	List<Planet> r = new ArrayList<Planet>();
	for (Planet p : planets) {
	    if (p.Owner() >= 2) {
		r.add(p);
	    }
	}
	return r;
    }

    // Return a list of all the planets that are not owned by the current
    // player. This includes all enemy planets and neutral planets.
    public List<Planet> NotMyPlanets() {
	List<Planet> r = new ArrayList<Planet>();
	for (Planet p : planets) {
	    if (p.Owner() != 1) {
		r.add(p);
	    }
	}
	return r;
    }

    // Return a list of all the fleets.
    public List<Fleet> Fleets() {
	List<Fleet> r = new ArrayList<Fleet>();
	for (Fleet f : fleets) {
            r.add(f);
	}
	return r;
    }

    // Return a list of all the fleets owned by the current player.
    public List<Fleet> MyFleets() {
	List<Fleet> r = new ArrayList<Fleet>();
	for (Fleet f : fleets) {
	    if (f.Owner() == 1) {
		r.add(f);
	    }
	}
	return r;
    }

    // Return a list of all the fleets owned by enemy players.
    public List<Fleet> EnemyFleets() {
	List<Fleet> r = new ArrayList<Fleet>();
	for (Fleet f : fleets) {
	    if (f.Owner() != 1) {
		r.add(f);
	    }
	}
	return r;
    }

    // Returns the distance between two planets, rounded up to the next highest
    // integer. This is the number of discrete time steps it takes to get
    // between the two planets.
    public int Distance(Planet sourcePlanet, Planet destinationPlanet) {
    	int srcID = sourcePlanet.PlanetID();
    	int destID = destinationPlanet.PlanetID();
    	if (distances[srcID][destID] != 0) return distances[srcID][destID];
    	else {
			double dx = sourcePlanet.X() - destinationPlanet.X();
			double dy = sourcePlanet.Y() - destinationPlanet.Y();
			int dist = (int)Math.ceil(Math.sqrt(dx * dx + dy * dy));
			distances[srcID][destID] = dist;
			distances[destID][srcID] = dist;
			return dist;
    	}
    }
    
    public int Distance(int sourcePlanet, int destinationPlanet) {
    	if (distances[sourcePlanet][destinationPlanet] != 0) return distances[sourcePlanet][destinationPlanet];
    	else {
    		Planet source = planets.get(sourcePlanet);
    		Planet destination = planets.get(destinationPlanet);
			double dx = source.X() - destination.X();
			double dy = source.Y() - destination.Y();
			int dist = (int)Math.ceil(Math.sqrt(dx * dx + dy * dy));
			distances[sourcePlanet][destinationPlanet] = dist;
			distances[destinationPlanet][sourcePlanet] = dist;
			return dist;
    	}
    }
    
    /**
     * Usable for a lot of distance comparing because doesn't do heavy sqrt or divisions.
     * @param sourcePlanet
     * @param destinationPlanet
     * @return Returns a comparable value but not a real distance.
     */
    public double DistanceCompare(Planet sourcePlanet, Planet destinationPlanet) {
    	double dx = destinationPlanet.X() - sourcePlanet.X();
    	double dy = destinationPlanet.Y() - sourcePlanet.Y();
    	return ((dx*dx)+(dy*dy));
    }
    
    public double DistanceCompare(int sourcePlanet, int destinationPlanet) {
    	return DistanceCompare(planets.get(sourcePlanet), planets.get(destinationPlanet));
    }
    
    public double DistanceCompare(Planet planet, double x, double y) {
    	double dx = x - planet.X();
    	double dy = y - planet.Y();
    	return ((dx*dx)+(dy*dy));
    }
    
    public int PayOff(Planet sourcePlanet, Planet destinationPlanet) {
    	//System.err.println("PayOff calling Distance(...");
    	int dist = Distance(sourcePlanet, destinationPlanet);
    	int sr = ShipsRequired(destinationPlanet, dist);
    	double gr = destinationPlanet.GrowthRate();
    	if (gr > 0) return dist + (int)(Math.ceil((sr / gr)));
    	else return Integer.MAX_VALUE;
    	//return 0;
    }
    
    public int PayOff(int sourcePlanet, int destinationPlanet) {
    	return PayOff(GetPlanet(sourcePlanet), GetPlanet(destinationPlanet));
    }
    
    public int ShipsRequired(Planet sourcePlanet, Planet destinationPlanet) {
    	int gr = (destinationPlanet.Owner() == 2) ? destinationPlanet.GrowthRate() : 0;
    	if (gr > 0) return destinationPlanet.NumShips() + (Distance(sourcePlanet, destinationPlanet) * gr) + 1;
    	else return destinationPlanet.NumShips() + 1;
    }
    
    public int ShipsRequired(Planet destinationPlanet, int distance) {
    	int gr = (destinationPlanet.Owner() == 2) ? destinationPlanet.GrowthRate() : 0;
    	return destinationPlanet.NumShips() + (distance * gr) + 1;
    }
    
    public int ShipsRandom(int numShipsAvailable) {
    	double percMin = Gene(7);
    	double percMax = Gene(8);
    	int numShips = (int)(percMin * numShipsAvailable + (Math.random() * ((percMax * numShipsAvailable) - (percMin * numShipsAvailable))));
    	return numShips;
    }
    
    public int ShipsRandom(int numShipsAvailable, int minimum) {
    	double percMax = Gene(8);
    	int numShips = (int)(minimum + (Math.random() * ((percMax * numShipsAvailable) - (minimum))));
    	return numShips;
    }
    
    public double Score(Planet srcP, Planet destP) {
    	double dist = DistanceCompare(srcP, destP);
    	double a = Gene(0);
    	double b = Gene(1);
    	double c = 0;							// If Own Planet to reinforce
    	if (destP.Owner() == 2) c = Gene(2);	// If Enemy Planet
    	else if (destP.Owner() == 0) c = 0.5 * Gene(2);		// If Neutral Planet
    	
    	double score = (a * destP.GrowthRate()) - (b * dist) - (c * destP.NumShips());
    	destP.setScoreTotal(score);
    	return score;
    }
    
    public double Score(int sourcePlanet, int destinationPlanet) {
    	return Score(GetPlanet(sourcePlanet), GetPlanet(destinationPlanet));
    }
    
    

    // Sends an order to the game engine. An order is composed of a source
    // planet number, a destination planet number, and a number of ships. A
    // few things to keep in mind:
    //   * you can issue many orders per turn if you like.
    //   * the planets are numbered starting at zero, not one.
    //   * you must own the source planet. If you break this rule, the game
    //     engine kicks your bot out of the game instantly.
    //   * you can't move more ships than are currently on the source planet.
    //   * the ships will take a few turns to reach their destination. Travel
    //     is not instant. See the Distance() function for more info.
    public void IssueOrder(int sourcePlanet,
                           int destinationPlanet,
                           int numShips) {
        System.out.println("" + sourcePlanet + " " + destinationPlanet + " " +
          numShips);
	System.out.flush();
    }

    // Sends an order to the game engine. An order is composed of a source
    // planet number, a destination planet number, and a number of ships. A
    // few things to keep in mind:
    //   * you can issue many orders per turn if you like.
    //   * the planets are numbered starting at zero, not one.
    //   * you must own the source planet. If you break this rule, the game
    //     engine kicks your bot out of the game instantly.
    //   * you can't move more ships than are currently on the source planet.
    //   * the ships will take a few turns to reach their destination. Travel
    //     is not instant. See the Distance() function for more info.
    public void IssueOrder(Planet source, Planet dest, int numShips) {
        System.out.println("" + source.PlanetID() + " " + dest.PlanetID() +
          " " + numShips);
	System.out.flush();
    }

    // Sends the game engine a message to let it know that we're done sending
    // orders. This signifies the end of our turn.
    public void FinishTurn() {
		System.out.println("go");
		System.out.flush();
    }

    // Returns true if the named player owns at least one planet or fleet.
    // Otherwise, the player is deemed to be dead and false is returned.
    public boolean IsAlive(int playerID) {
		for (Planet p : planets) {
		    if (p.Owner() == playerID) {
			return true;
		    }
		}
		for (Fleet f : fleets) {
		    if (f.Owner() == playerID) {
			return true;
		    }
		}
		return false;
    }

    // If the game is not yet over (ie: at least two players have planets or
    // fleets remaining), returns -1. If the game is over (ie: only one player
    // is left) then that player's number is returned. If there are no
    // remaining players, then the game is a draw and 0 is returned.
    public int Winner() {
		Set<Integer> remainingPlayers = new TreeSet<Integer>();
		for (Planet p : planets) {
		    remainingPlayers.add(p.Owner());
		}
		for (Fleet f : fleets) {
		    remainingPlayers.add(f.Owner());
		}
		switch (remainingPlayers.size()) {
		case 0:
		    return 0;
		case 1:
		    return ((Integer)remainingPlayers.toArray()[0]).intValue();
		default:
		    return -1;
		}
    }

    // Returns the number of ships that the current player has, either located
    // on planets or in flight.
    public int NumShips(int playerID) {
		int numShips = 0;
		for (Planet p : planets) {
		    if (p.Owner() == playerID) {
			numShips += p.NumShips();
		    }
		}
		for (Fleet f : fleets) {
		    if (f.Owner() == playerID) {
			numShips += f.NumShips();
		    }
		}
		return numShips;
    }

    // Parses a game state from a string. On success, returns 1. On failure,
    // returns 0.
    private int ParseGameState(String s) {
	planets.clear();
	fleets.clear();
	int planetID = 0;
	String[] lines = s.split("\n");
	for (int i = 0; i < lines.length; ++i) {
	    String line = lines[i];
	    int commentBegin = line.indexOf('#');
	    if (commentBegin >= 0) {
		line = line.substring(0, commentBegin);
	    }
	    if (line.trim().length() == 0) {
		continue;
	    }
	    String[] tokens = line.split(" ");
	    if (tokens.length == 0) {
		continue;
	    }
	    if (tokens[0].equals("P")) {
		if (tokens.length != 6) {
		    return 0;
		}
		double x = Double.parseDouble(tokens[1]);
		double y = Double.parseDouble(tokens[2]);
		int owner = Integer.parseInt(tokens[3]);
		int numShips = Integer.parseInt(tokens[4]);
		int growthRate = Integer.parseInt(tokens[5]);
		Planet p = new Planet(planetID++,
				      owner,
				      numShips,
				      growthRate,
				      x, y);
		planets.add(p);
	    } else if (tokens[0].equals("F")) {
		if (tokens.length != 7) {
		    return 0;
		}
		int owner = Integer.parseInt(tokens[1]);
		int numShips = Integer.parseInt(tokens[2]);
		int source = Integer.parseInt(tokens[3]);
		int destination = Integer.parseInt(tokens[4]);
		int totalTripLength = Integer.parseInt(tokens[5]);
		int turnsRemaining = Integer.parseInt(tokens[6]);
		Fleet f = new Fleet(owner,
				    numShips,
				    source,
				    destination,
				    totalTripLength,
				    turnsRemaining);
		fleets.add(f);
	    } else {
		return 0;
	    }
	}
	return 1;
    }

    // Loads a map from a text file. The text file contains a description of
    // the starting state of a game. See the project wiki for a description of
    // the file format. It should be called the Planet Wars Point-in-Time
    // format. On success, return 1. On failure, returns 0.
    private int LoadMapFromFile(String mapFilename) {
		String s = "";
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(mapFilename));
			int c;
			while ((c = in.read()) >= 0) {
			    s += (char)c;
			}
		} catch (Exception e) {
		    return 0;
		} finally {
		    try {
			in.close();
		    } catch (Exception e) {
			// Fucked.
		    }
		}
		return ParseGameState(s);
    }

    
}
