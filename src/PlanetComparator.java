

import java.util.Comparator;

public class PlanetComparator implements Comparator<Planet> {
	
	private int type;
	
	public PlanetComparator() {
		super();
		this.type = 0;
	}
	
	/**
	 * 
	 * @param type_ 	0 = Scores,
	 * 					1 = GrowthRate,
	 * 					2 = Distance,
	 * 					3 = Distance Comparable,
	 * 					4 = PayOff.
	 */
	public PlanetComparator(int type_) {
		super();
		this.type = type_;
	}
	
	@Override
    public int compare(Planet p1, Planet p2) {
		switch (type) {
		default:
		case 0:
			if (p1.ScoreTotal() < p2.ScoreTotal()) return 1;
	        else if (p1.ScoreTotal() > p2.ScoreTotal()) return -1;
	        //else {
	        	//if (p1.Score() < p2.Score()) return 1;
	            //else if (p1.Score() > p2.Score()) return -1;
	            else return 0;
	        //}
			
		case 1:
			if (p1.GrowthRate() < p2.GrowthRate()) return 1;
	        else if (p1.GrowthRate() > p2.GrowthRate()) return -1;
	        else return 0;
		
		case 2:
			if (p1.Dist() > p2.Dist()) return 1;
	        else if (p1.Dist() < p2.Dist()) return -1;
	        else return 0;
			
		case 3:
			if (p1.distC > p2.distC) return 1;
	        else if (p1.distC < p2.distC) return -1;
	        else return 0;
			
		case 4:
			if (p1.payoff > p2.payoff) return 1;
	        else if (p1.payoff < p2.payoff) return -1;
	        else return 0;
		}

    }
}
