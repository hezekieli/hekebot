

import java.text.DecimalFormat;
import java.util.*;
import java.io.*;
import ea.*;

/*****************************************************************
 * HekeBot2
 * Is amazing and amazingly simple! Like all the best things are.
 * 
 * It simply 
 * - sends 1 fleet per turn 
 * - from the strongest planet
 * - half the available ships
 * - if there's at least 50 ships
 * and to a planet with best score calculated according to
 * - closest distance
 * - least ships
 * - best growthrate
 * 
 * Problems ensue when there are not very high growth rate planets. 
 * Another thing to improve is accelerating the fleets sent when
 * gaining more planets.
 * 
 * @author hemijupe
 *
 */

public class MyBot {
    // The DoTurn function is where your code goes. The PlanetWars object
    // contains the state of the game, including information about all planets
    // and fleets that currently exist. Inside this function, you issue orders
    // using the pw.IssueOrder() function. For example, to send 10 ships from
    // planet 3 to planet 8, you would say pw.IssueOrder(3, 8, 10).
    //
    // There is already a basic strategy in place here. You can use it as a
    // starting point, or you can throw it out entirely and replace it with
    // your own. Check out the tutorials and articles on the contest website at
    // http://www.ai-contest.com/resources.
    public static void DoTurn(PlanetWars pw, String[] args) throws Exception {  	

    	long startTime = System.nanoTime();
    	long elapsedTime = (System.nanoTime() - startTime) / 1000000;
    	DecimalFormat df = new DecimalFormat("0.00");
    	//System.err.println(elapsedTime + " StartTime~");
    	
    	// PrintMode (to optimize when playing multiple matches)
    	boolean printMode = false;
    	if (args.length > 0) {
    		if (Integer.parseInt(args[0]) == 1) printMode = true;
    	}
    	
    	// Fire up the writer
    	PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("HekeBotMatchData.txt", true)));
    	String toPrint = "";	// String used to compose prints for both System.err and writer
    	//writer.println("PrintMode = " + printMode + "(" + args[0] + ")");
    	
    	/*// Genes
    	
    	int[] genes = {30,70,6};			// Better Default values
    	
    	//int genomeLength = (args.length > 3) ? args.length : 3;

    	
    	// Extract the genome
    	if (printMode) toPrint = "This bots args("+args.length+"): [ ";
    	
    	for (int i = 0; i < args.length; i++) {
    		if (printMode) toPrint += args[i]+ " ";
    		if (i > 0) {											// First argument is used by printMode
    			int gene = Integer.parseInt(args[i]);
    			if (gene >= 0 && gene <=  100) genes[i-1] = gene;	// To check that the genes value is in valid range
    		}
    	}
    	
    	// print it if printMode on
    	if (printMode) {
	    	toPrint += "] \n";
	    	toPrint += "This bots genes: [ ";
	    	
	    	for (int i = 0; i < genes.length; i++) {
	    		toPrint += genes[i]+ " ";
	    	}
	    	toPrint += "]";
	    	writer.println(toPrint);
    	}
    	*/
    	
    	
    	    	
    	// No point planning for orders if you don't have any planets left...
    	if (pw.IsAlive(1) && !pw.MyPlanets().isEmpty()) {
   		
    		/*****************
	    	 * MACRO
	    	 * Figure out Game State
	    	 * GrowthRate lead
	    	 * Fleet lead
	    	 * Planet lead
	    	 * Planet Statuses
	    	 *****************/
    		
    		
    		// Game State is determined based on how many % of neutral planets left
    		int stateGame;		// 0 = Early 	= expand unless enemy very near
								// 1 = Mid 		= expand if easy, in ship lead and behind in gr
    							// 2 = late 	= Mainly attack unless very clear ship lead
    		if ((((double)pw.NeutralPlanets().size()) / ((double)pw.Planets().size())) > 0.7) stateGame = 0;		// GENE participant
    		else if ((((double)pw.NeutralPlanets().size()) / ((double)pw.Planets().size())) > 0.3) stateGame = 1;	// GENE participant
    		else stateGame = 2;
    		
    		//elapsedTime = (System.nanoTime() - startTime) / 1000000;
    		//System.err.println(elapsedTime + " Game State = " + stateGame);
    		
    		/**
    		 * WHAT IN GENERAL SHOULD DO
			 * - 3 Overwhelm	when in clear lead (gr and fleets) -> no point holding back, push from everywhere put also keep reinforcing
			 * - 2 Attack		when behind in gr but lead in ships -> hurt and gain back the gr lead
			 * - 1 Defend		when in gr lead and behind on ships -> buy time to get ship lead
			 * - 0 Expand		Normal, balanced, even state or behind
    		 */
    		
    		// Based on Game State and situation
    		int stateMacro = 0;
    		
    		if (pw.grLeadPerc > pw.Gene(9) && pw.shipLeadPerc > pw.Gene(10)) {	// Clear Lead	GENE PARTICIPANT
    			//if ((pw.EnemyPlanets().size()/pw.Planets().size()) < 0.1) stateMacro = 4;	// Enemy has < 10% of planets
    			//else stateMacro = 3;
    			stateMacro = 3;
    		}
    		else if (pw.shipLeadPerc > pw.Gene(12) && pw.grLeadPerc < -pw.Gene(11)) {	// Ship lead, no gr lead -> Attack	GENE PARTICIPANT
    			stateMacro = 2;
    		}
    		else if (pw.grLeadPerc > pw.Gene(11) && pw.shipLeadPerc < -pw.Gene(12)) {	// GR lead, no ship lead -> Defend	GENE PARTICIPANT
    			stateMacro = 1;
    		}
    		else {
    			stateMacro = 0;
    			// Need to figure out how to know if we are too close to the enemy to go expanding...
    			//if ((pw.distanceStart / pw.distanceMax) < 0.3) stateMacro = 2;
    			//else stateMacro = 0;
    		}
    		
    		//elapsedTime = (System.nanoTime() - startTime) / 1000000;
    		//System.err.println(elapsedTime + " Macro State = " + stateMacro);
    		
    	    Planet base = null;
    	    Planet target = null;
    		
    	    // Deciding the base planet
			//elapsedTime = (System.nanoTime() - startTime) / 1000000;
			//System.err.println(elapsedTime + " Deciding base planet...");
			double dMin = Double.MIN_VALUE;
			double dOwn = Double.MIN_VALUE;
			double dEnemy = Double.MIN_VALUE;
			double dCenter = Double.MIN_VALUE;
			for (Planet p : pw.MyPlanets()) {
				dOwn = pw.DistanceCompare(p, pw.centOfGP1[0], pw.centOfGP1[1]);
				dEnemy = pw.DistanceCompare(p, pw.centOfGP2[0], pw.centOfGP2[1]);
				dCenter = pw.DistanceCompare(p, pw.centOfGAll[0], pw.centOfGAll[1]);
				double d = (pw.Gene(13) * dOwn) + (pw.Gene(14) * dEnemy) + (pw.Gene(15) * dCenter);			// GENE PARTICIPANT
				if (dMin == Double.MIN_VALUE || d < dMin) {
					dMin = d;
					base = p;	// Planet base = Planet p;
					base.setIsBase(true);
				}
			}
			
			System.err.println("Base = " + base.PlanetID());
			
			// Deciding the target planet
			if (!pw.EnemyPlanets().isEmpty()) {
				//elapsedTime = (System.nanoTime() - startTime) / 1000000;
			//System.err.println(elapsedTime + " Deciding target planet...");
				double dMax = Double.MAX_VALUE;
				for (Planet p : pw.EnemyPlanets()) {
					dOwn = pw.DistanceCompare(p, base);
					dEnemy = pw.DistanceCompare(p, pw.centOfGP2[0], pw.centOfGP2[1]);
					double d = (pw.Gene(17) * dEnemy) - (pw.Gene(16) * dOwn) + (pw.Gene(18) * p.GrowthRate());		// GENE PARTICIPANT
					if (dMax == Double.MAX_VALUE || d > dMax) {
						dMax = d;
						target = p;
						target.setIsTarget(true);
					}
				}
			}
    		
			if (target != null)	System.err.println("Target = " + target.PlanetID());
			System.err.println("Target = null");
    		
    		
    		/***************************
        	 * WRITE info about planets
        	 ***************************/
    		if (printMode) {
	    		writer.println("");
	    		//double percNeutral = 0.01 * (int)(100*((double)pw.NeutralPlanets().size()) / ((double)pw.Planets().size()));
	    		String percNeutral = df.format(((double)pw.NeutralPlanets().size()) / ((double)pw.Planets().size()));
	    		//System.err.println("Game State = " + stateGame + "("+percNeutral+"), Macro State = " + stateMacro);
	    		writer.println("Game \t\tMacro \tGRLead \tSLead\t");
	    		writer.println(stateGame+" ("+percNeutral+") \t"+stateMacro+" \t"+df.format(pw.grLeadPerc)+" \t"+df.format(pw.shipLeadPerc)+"\t");
	    		
	    		if (1 == 0) {
	    			writer.println("Game State = " + stateGame + "("+percNeutral+"), Macro State = " + stateMacro);
		    		toPrint = "GR Lead: " + pw.grP1 + "-" + pw.grP2 + "  (" + pw.grLeadPerc + ")\t Ships Lead: " + pw.shipsP1 + "-" + pw.shipsP2 + "  (" + pw.shipLeadPerc + ")";
		    		writer.println(toPrint);
		    		writer.println(" ID\t Owner\t GR\t Ships\t SBal\t SAtt\t SReinf\t Score");
		    		//System.err.println(toPrint);
		        	for (Planet p : pw.Planets()) {
		    			String o = " - ";
		    			if (p.Owner() == 1) o = "(1)";
		    			else if (p.Owner() == 2) o = " 2 ";
		    			
		    			double score = (int)p.ScoreTotal();//0.01 * ((int)(p.Score() * 100));
		
		    			String strIsBase = (p == base) ? "Base" : "";
		    			String strIsTarget = (p == target) ? "Target" : "";
		    			//toPrint = "Planet " + p.PlanetID() + "\t  Ships: " + p.NumShips() + "  \t(" + p.ShipsBalance() + ")\t ["+p.ShipsAttacking()+"]\t {"+p.ShipsReinforcing()+"}\t GR: " +p.GrowthRate() + "\t Score: " + score + "\t Owner: " + o + "\t " + strIsSrc + "\t " + strIsDest;
		    			toPrint = " " + p.PlanetID() + "\t " + o + "\t " + p.GrowthRate() + "\t " + p.NumShips() + "\t (" + p.ShipsBalance() + ")\t ["+p.ShipsAttacking()+"]\t {"+p.ShipsReinforcing()+"}\t Score: " + score + "\t " + strIsBase + "\t " + strIsTarget;
		    			writer.println(toPrint);
		    			//System.err.println(toPrint);
		    		}
	    		}
    		}
        	
    		//elapsedTime = (System.nanoTime() - startTime) / 1000000;
    		//System.err.println(elapsedTime + " Planet info written");
    		
    		
    		
    		
    		
    		/*****************
	    	 * MICRO
	    	 * Compose Orders
	    	 *****************/	
    		
    		
	    	
    		//writer.println("Starting to compose orders...");
    		//System.err.println("Starting to compose orders...");
	    	
    		List<Order> orders = new ArrayList<>();
    		
    		//boolean bugsearch = false;
    		//if (bugsearch == false) {
    		// Loop through MyPlanets 
	    	for (Planet srcP : pw.MyPlanets()) {
	    	
	    		// Current Source Planet
	    		int srcID = srcP.PlanetID();
	    		boolean isBase = (srcP == base)? true : false;
	    		boolean destIsBase;
	    		boolean destIsTarget;
	    		
	    		int dist = 0;
    			int destID = -1;
	    		Planet destP = null;
	    		int numShips = 0;
	    		int numShipsRequired = 0;
	    		int numShipsOnSrc = 0;
	    		
	    		//System.err.println("Setting up Planet " + srcID + "...");
	    		
	    		// Need to figure out what this planet should do
	    			// Overwhelm	send multiple fleets to enemy planets
	    			// Attack		send fleet to enemy planet
	    			// Harrass
	    			// Explode		send multiple fleets to neutral planets
	    			// Expand		send fleet to neutral planet
	    			// Reinforce	send to own planet
	    			// Defend		no orders
	    			// CallForHelp	inform neighbours about need for help
	    		
	    		
	    		// What to do if base planet
	    			// Don't list MyPlanets
	    			// If state=attack -> list only enemyplanets
	    			// Only base planet can perform multiple orders per turn
	    			// If GameState>=mid -> always hit target
	    		
	    		// What to do if colony planet
	    			// List among all Planets
	    				// Base planet
	    				// But other own planet only if needs reinforcements
	    				// If state=attack -> no neutral planets
	    			// Check closest
	    				// If base -> tithe
	    				// If target && midGame -> attack
	    				// If own -> reinforce
	    				// If neutral && state != attack && enough to conquer -> conquer
	    				// If enemy -> attack
	    			
	    		
	    		
	    		String isItBase = (srcP.IsBase()) ? "(Base)" : "";
	    		System.err.println("Planet " + srcID + isItBase + ": Making a TempList..."); 
	    		
	    		/********************************
		    	 * Score all the planets
		    	 * Scored options are:
		    	 * 	- some enemy planet
		    	 * 	- some neutral planet
		    	 * 	- some own planet to reinforce
		    	 * 	- stay and defend
		    	 ********************************/
	    		List<Planet> tempPlanets = new ArrayList<Planet>();
	    		
	    		// If is Base planet
	    		if (isBase) {
	    			// = Overwhelm
	    			if (stateMacro == 3) {
	    				for (Planet p : pw.EnemyPlanets()) {
	    					if (p != target) {
		    					p.distC = pw.DistanceCompare(srcP, p);
	    						//p.payoff = pw.PayOff(srcID, p.PlanetID());
	    						pw.Score(srcP, p);
	    						tempPlanets.add(p);
		    				}
	    				}	// for
	    				Collections.sort(tempPlanets, new PlanetComparator(0));		// Score
		    			if (target != null) {
		    				target.distC = pw.DistanceCompare(srcP, target);
		    				//target.payoff = pw.PayOff(srcID, target.PlanetID());
			    			tempPlanets.add(0, target);
		    			}
		    			int max = pw.GeneInt(4);	// Max orders (Base)
	    				//int max = (pw.GeneInt(4) < tempPlanets.size()) ? pw.GeneInt(4) : tempPlanets.size();
	    				if (pw.GeneInt(4) < tempPlanets.size()) {
	    					for (int i = max +1; i < tempPlanets.size(); i++) {
	    						tempPlanets.remove(i);
	    					}
	    				}
	    				for (Planet p : pw.MyPlanets()) {
	    					if (p != srcP && p.ShipsAttacking() > 0) {
	    						p.distC = pw.DistanceCompare(srcP, p);
	    						tempPlanets.add(p);
	    					}
	    				}
	    				Collections.sort(tempPlanets, new PlanetComparator(3));		// DistanceC
	    			}
	    			// = Attack
	    			else if (stateMacro == 2) {	
	    				for (Planet p : pw.EnemyPlanets()) {
	    					if (p != target) {
		    					p.distC = pw.DistanceCompare(srcP, p);
	    						p.payoff = pw.PayOff(srcID, p.PlanetID());
	    						tempPlanets.add(p);
		    				}
	    				}
	    				Collections.sort(tempPlanets, new PlanetComparator(0));		// Score
		    			if (target != null) {
		    				target.distC = pw.DistanceCompare(srcP, target);
		    				target.payoff = pw.PayOff(srcID, target.PlanetID());
			    			tempPlanets.add(0, target);
		    			}
	    			}
	    			// = Defend
	    			else if (stateMacro == 1) {		
	    				for (Planet p : pw.MyPlanets()) {
	    					if (p != srcP && p.ShipsAttacking() > 0) {
	    						p.distC = pw.DistanceCompare(srcP, p);
	    						tempPlanets.add(p);
	    					}
	    				}
	    				Collections.sort(tempPlanets, new PlanetComparator(3));		// DistanceC
	    			}
	    			// stateMacro = 0 = Expand (Normal)
	    			else { 
	    				for (Planet p : pw.NotMyPlanets()) {
	    					pw.Score(srcP, p);
	    					tempPlanets.add(p);
	    				}
	    				Collections.sort(tempPlanets, new PlanetComparator(0));		// Score
	    			}
	    			
	    		}
	    		// If Colony planet
	    			// (state = 0,2,3) First figure out couple (gene defined number) of the best neutral/myplanets
	    				// Sorting them according to score
	    			// Then add base and reinforcable planets
	    		else {
	    			
	    			// Only send ships if we have some for spare
	    			if (srcP.ShipsAttacking() < srcP.NumShips()) {
	    				// = Overwhelm
		    			if (stateMacro == 3) {
		    				for (Planet p : pw.EnemyPlanets()) {
		    					if (p != target) {
			    					p.distC = pw.DistanceCompare(srcP, p);
		    						//p.payoff = pw.PayOff(srcID, p.PlanetID());
		    						pw.Score(srcP, p);
		    						tempPlanets.add(p);
			    				}
		    				}	// for
		    				Collections.sort(tempPlanets, new PlanetComparator(0));		// Score
			    			if (target != null) {
			    				target.distC = pw.DistanceCompare(srcP, target);
			    				//target.payoff = pw.PayOff(srcID, target.PlanetID());
				    			tempPlanets.add(0, target);
			    			}
			    			int max = pw.GeneInt(4);	// Max orders (Base)
		    				//int max = (pw.GeneInt(4) < tempPlanets.size()) ? pw.GeneInt(4) : tempPlanets.size();
		    				if (pw.GeneInt(4) < tempPlanets.size()) {
		    					for (int i = max +1; i < tempPlanets.size(); i++) {
		    						tempPlanets.remove(i);
		    					}
		    				}
		    				for (Planet p : pw.MyPlanets()) {
		    					if (p != srcP && p.ShipsAttacking() > 0) {
		    						p.distC = pw.DistanceCompare(srcP, p);
		    						tempPlanets.add(p);
		    					}
		    				}
		    				Collections.sort(tempPlanets, new PlanetComparator(3));		// DistanceC
		    			}
		    			// = Attack
		    			else if (stateMacro == 2) {
		    				for (Planet p : pw.MyPlanets()) {
		    					if (p != srcP) {
		    						if (p == base){
			    						if (stateMacro == 0 || stateMacro == 2) {
			    							p.distC = pw.DistanceCompare(srcP, p);
			    							tempPlanets.add(p);
			    						}
			    						else {
			    							if (p.ShipsAttacking() > 0) {
			    								p.distC = pw.DistanceCompare(srcP, p);
				    							tempPlanets.add(p);
			    							}
			    						}
			    					}
			    					else if (p.ShipsAttacking() > 0) {
			    						p.distC = pw.DistanceCompare(srcP, p);
			    						tempPlanets.add(p);
			    					}
		    					}
		    				}
		    				if (target != null) {
			    				target.distC = pw.DistanceCompare(srcP, target);
				    			tempPlanets.add(0, target);
		    				}
		    				Collections.sort(tempPlanets, new PlanetComparator(3));
		    			}
		    			// = Defend
		    			else if (stateMacro == 1) {
		    				for (Planet p : pw.MyPlanets()) {
		    					if (p != srcP) {
		    						if (p.ShipsAttacking() > 0) {
			    						p.distC = pw.DistanceCompare(srcP, p);
			    						tempPlanets.add(p);
		    						}
		    					}
		    				}
		    				Collections.sort(tempPlanets, new PlanetComparator(3));
		    			}
		    			// stateMacro = 0 = Expand (Normal)
		    			else {
		    				for (Planet p : pw.NotMyPlanets()) {
		    					if (p != target) {
			    					p.distC = pw.DistanceCompare(srcP, p);
		    						pw.Score(srcP, p);
		    						tempPlanets.add(p);
			    				}
		    				}	// for
		    				Collections.sort(tempPlanets, new PlanetComparator(0));		// Score
			    			if (target != null) {
			    				target.distC = pw.DistanceCompare(srcP, target);
				    			tempPlanets.add(0, target);
			    			}
			    			int max = pw.GeneInt(3);	// Max orders (Colony)
		    				if (pw.GeneInt(3) < tempPlanets.size()) {
		    					for (int i = max +1; i < tempPlanets.size(); i++) {
		    						tempPlanets.remove(i);
		    					}
		    				}
		    				for (Planet p : pw.MyPlanets()) {
		    					if (p != srcP) {
		    						if (p == base){
			    						if (stateMacro == 0 || stateMacro == 2) {
			    							p.distC = pw.DistanceCompare(srcP, p);
			    							tempPlanets.add(p);
			    						}
			    						else {
			    							if (p.ShipsAttacking() > 0) {
			    								p.distC = pw.DistanceCompare(srcP, p);
				    							tempPlanets.add(p);
			    							}
			    						}
			    					}
			    					else if (p.ShipsAttacking() > 0) {
			    						p.distC = pw.DistanceCompare(srcP, p);
			    						tempPlanets.add(p);
			    					}
		    					}
		    				}
		    				Collections.sort(tempPlanets, new PlanetComparator(3));
		    			}
		    			
	    				
		    			/*
		    			for (Planet p : pw.Planets()) {		    			
			    			
			    			if (p != srcP) {			// If not the current source Planet
			    				// If Own Planet
			    				if (p.Owner() == 1) {
			    					if (p == base){
			    						if (stateMacro == 0 || stateMacro == 2) {
			    							p.distC = pw.DistanceCompare(srcP, p);
			    							tempPlanets.add(p);
			    						}
			    						else {
			    							if (p.ShipsAttacking() > 0) {
			    								p.distC = pw.DistanceCompare(srcP, p);
				    							tempPlanets.add(p);
			    							}
			    						}
			    						
			    					}
			    					else if (p.ShipsAttacking() > 0) {
			    						p.distC = pw.DistanceCompare(srcP, p);
			    						tempPlanets.add(p);
			    					}
			    				}
			    				// If Neutral Planet
			    				if (p.Owner() == 0) {
			    					if (stateMacro == 0) {	// If normal state (expanding, even, or behind)
			    						p.distC = pw.DistanceCompare(srcP, p);
			    						p.payoff = pw.PayOff(srcID, p.PlanetID());
			    						tempPlanets.add(p);
			    					}
			    				}
			    				// If Enemy Planet
			    				if (p.Owner() == 2) {
			    					if (stateMacro != 1) {	// If not defend state
				    					p.distC = pw.DistanceCompare(srcP, p);
			    						p.payoff = pw.PayOff(srcID, p.PlanetID());
			    						tempPlanets.add(p);
			    					}
			    				}
			    			}
			    		}
			    		
			    		// Sort tempPlanets. StateMacro defines in what way
			    		Collections.sort(tempPlanets, new PlanetComparator(3));		// DistanceComparable
			    		*/
	    			}
	    		}
	    		
	    		
	    		System.err.println("TempList composed and sorted");
	    		
	    		
	    		
	    		
	    		if (printMode && 1 == 0) {
		    		// For printing the tempPlanets
		    		writer.println("");
		    		
		    		toPrint = "Sorted Destination Planets of Planet " + srcID + "\t  Ships: " + srcP.NumShips() + "  \t(" + srcP.ShipsBalance() + ")  \t ["+srcP.ShipsAttacking()+"]\t {"+srcP.ShipsReinforcing()+"}\t GR: " +srcP.GrowthRate();
		    		writer.println(toPrint);
		    		writer.println(" ID\t Owner\t GR\t Ships\t SBal\t SAtt\t SReinf\t Dist\t PayOff\t Score");
					//System.err.println(toPrint);
					
			    	for (Planet p : tempPlanets) {
		    			String o = " - ";
		    			if (p.Owner() == 1) o = "(1)";
		    			else if (p.Owner() == 2) o = " 2 ";
		    			dist = (int)p.distC;
		    			String distStr = (dist > 0) ? Integer.toString(dist) : "-";
		    			//double score = (int)p.ScoreTotal();//0.01 * ((int)(p.Score() * 100));

		    			//toPrint = "Planet " + p.PlanetID() + "\t  Ships: " + p.NumShips() + "  \t(" + p.ShipsBalance() + ")  \t ["+p.ShipsAttacking()+"]\t {"+p.ShipsReinforcing()+"}\t GR: " +p.GrowthRate() + "\t Score: " + score + "\t Owner: " + o;
		    			toPrint = " " + p.PlanetID() + "\t " + o + "\t " + p.GrowthRate() + "\t " + p.NumShips() + "\t (" + p.ShipsBalance() + ")\t ["+p.ShipsAttacking()+"]\t {"+p.ShipsReinforcing()+"}\t " + distStr + "\t " + p.payoff;// + "\t " + score;
		    			writer.println(toPrint);
		    			//System.err.println(toPrint);
		    		}
	    		}
	    		
	    		System.err.println("Sorted info written.");
		    	
		    	/****************************************************************
		    	 * While loop for checking whether we should 
		    	 * create another order for current source Planet
		    	 ****************************************************************/
	    		int numOfOrders = 0;
	    		int numOfOrdersMax = pw.GeneInt(3);			// GENE PARTICIPANT
	    		if (isBase) numOfOrdersMax = pw.GeneInt(4);	// GENE PARTICIPANT
	    		int numOfPlanetsHandled = 0;
	    		int numOfPlanetsHandledMax = pw.GeneInt(5);				// GENE PARTICIPANT
	    		if (isBase) numOfPlanetsHandledMax = pw.GeneInt(6); 	// GENE PARTICIPANT
	    		//int minimumScore = 0;
	    		//setMoreOrders = false;
	    		boolean setMoreOrders = true;
	    		
	    		// StateMacro and StateGame should define how many orders may be given per planet
	    		while (	!tempPlanets.isEmpty() && 					
    					numOfPlanetsHandled < numOfPlanetsHandledMax &&			// To limit going through the whole list (sticking to the best and acting only then)
    					numOfOrders < numOfOrdersMax) 
	    		{
	    			
	    			//System.err.println("tempPlanets: " + tempPlanets.size());
	    			
	    			destP = tempPlanets.get(0);
	    			destID = destP.PlanetID();
	    			
	    			// Remove current target planet to tell it has been dealt with
	    			tempPlanets.remove(0);
	    			numOfPlanetsHandled++;
	    			
	    			// Check to see if we should continue this iteration
	    			if (destID == srcID) break;

	    			
	    			
	    			
	    			// Number of ships
	    			
		    		numShipsOnSrc = srcP.NumShipsLeft() - srcP.ShipsAttacking();	// Check how many ships left after previous orders for this planet
		    		numShipsRequired = 0;					// To make sure not to use the information from the previous iteration
		    		numShips = 0;							// -''-
		    		//double genePercMin = 0.5 * (0.01 * genes[0]);
					//double genePercMax = 0.5 + (0.01 * genes[1]);
					double genePercMin = 0.3;		// GENE PARTICIPANT
					double genePercMax = 0.7;		// GENE PARTICIPANT
		    		
					// Hitting the Target from Base
					if (srcP == base && destP == target) {
						dist = pw.Distance(srcP,  destP);
						numShipsRequired = pw.ShipsRequired(destP, dist) + destP.ShipsAttacking();
						//numShipsRequired = (dist * destP.GrowthRate()) - destP.ShipsBalance() + 1;
						if (numShipsOnSrc > numShipsRequired) {
							numShips = numShipsRequired;
							//numShips = pw.ShipsRandom(numShipsOnSrc, numShipsRequired);
							//numShips = (int)(numShipsRequired + (Math.random() * (numShipsOnSrc - numShipsRequired)));
						}
						else numShips = pw.ShipsRandom(numShipsOnSrc);
							//numShips = (int)(genePercMin * numShipsOnSrc + (Math.random() * ((genePercMax * numShipsOnSrc) - (genePercMin * numShipsOnSrc))));
					}
					// Reinforcement and tithe
					else if (destP.Owner() == 1) {
						if (destP == base) {	// Tithe
							numShips = pw.ShipsRandom(numShipsOnSrc);
							//numShips = (int)(genePercMin * numShipsOnSrc + (Math.random() * ((genePercMax * numShipsOnSrc) - (genePercMin * numShipsOnSrc))));
						}
						else {		// Reinforcements
							numShipsRequired = 1 - destP.ShipsBalance();
							if (numShipsOnSrc > numShipsRequired) {
								numOfOrders = numShipsRequired;
							}
							else numShips = pw.ShipsRandom(numShipsOnSrc);
								//numShips = (int)(genePercMin * numShipsOnSrc + (Math.random() * ((genePercMax * numShipsOnSrc) - (genePercMin * numShipsOnSrc))));
						}
					}
					// 
					else if (destP.Owner() == 2) {
						dist = pw.Distance(srcP,  destP);
						numShipsRequired = pw.ShipsRequired(destP, dist) - destP.ShipsBalance();
						//numShipsRequired = (dist * destP.GrowthRate()) - destP.ShipsBalance() + 1;
						if (numShipsOnSrc > numShipsRequired) {
							numShips = numShipsRequired;
						}
						else if (destP == target || stateMacro == 3) {
							numShips = pw.ShipsRandom(numShipsOnSrc);
							//numShips = (int)(genePercMin * numShipsOnSrc + (Math.random() * ((genePercMax * numShipsOnSrc) - (genePercMin * numShipsOnSrc))));
						}
					}
					// If Neutral, send only if we have enough to conquer
					else {
						numShipsRequired = 1 - destP.ShipsBalance();
						if (numShipsOnSrc > numShipsRequired) {
							numShips = numShipsRequired;
						}
					}
    			
					System.err.println("Ready to Create order");
	    			
	    			// Create Order
		    		if (destID >= 0 && numShips > 0) {
		    			if (srcP.SubtractNumShipsLeft(numShips)) {
		    				destP.AddShipsBalance(numShips);
		    				destP.AddShipsReinforcing(numShips);
			    			Order order = new Order(srcID, destID, numShips, numShipsOnSrc);
							orders.add(order);
							numOfOrders++;
							destP.setIsTarget(true);
							System.err.println("Order created.");
		    			}
					}
		    		else System.err.println("No order created... :(");
	    			
		    		
		    		System.err.println("TempList.isEmpty() = " + tempPlanets.isEmpty() + ", size() = " + tempPlanets.size() + ", numOfPlanetsHandled = " + numOfPlanetsHandled);

	    		} // End of while (Set more targets)
	    		
	    		
	    		elapsedTime = (System.nanoTime() - startTime) / 1000000;
	    		System.err.println(elapsedTime + " End of While loop. Printing orders ("+orders.size()+")");
	    		
	    		/***************************
		    	 * Writing / Printing ORDERS
		    	 ***************************/
	    		if (printMode) {
			    	writer.println("Orders ("+numOfOrders+"): ");
			    	System.err.println("Orders ("+numOfOrders+"): ");
			    	int oIndex = 0;
			    	//int startIndex = orders.size() - numOfOrders;
			    	//int endIndex = startIndex + numOfOrders;
			    	//System.err.println("startIndex = " + startIndex + " and endIndex = " + endIndex);
			    	//for (int i = startIndex; i < endIndex; i++) { 
			    	for (Order o : orders) {
			    		if (o.src == srcID) {
				    		//Order o = orders.get(i);
				    		double percToSend = (int)(1000 * (double)o.numShips / (double)o.maxShips) / 10;
				    		/*
				    		if (o.maxShips > 0) {
				    			percToSend = (int)(1000 * (double)o.numShips / (double)o.maxShips) / 10;
				    		}
				    		else percToSend = 0;
				    		*/
				    		toPrint = "["+oIndex+"] " + o.src + " -> " + o.dest + " (" + o.numShips + "/" + o.maxShips + " - " + percToSend + " %)";
				    		writer.println(toPrint);
				    		System.err.println(toPrint);
				    		oIndex++;
			    		}
			    	}
	    		}
	    		
	    		System.err.println("Orders written.");
	    		
	    	}	// MyPlanets
    		//}
    		
	    	//System.err.println("Orders created.");
	    	//writer.println("Orders created.");
	    	
	    	/* Test printing
	    	writer.println("There are "+orders.size()+" orders after loop.");
	    	System.err.println("There are "+orders.size()+" orders after loop.");
			*/
    		
    		elapsedTime = (System.nanoTime() - startTime) / 1000000;
    		System.err.println(elapsedTime + " Closing writer...");

	    	writer.close();
	    	
	    	elapsedTime = (System.nanoTime() - startTime) / 1000000;
    		System.err.println(elapsedTime + " Issuing all the orders...("+orders.size()+")");
	    	
	    	
	    	/************************
	    	 * ISSUE ALL THE ORDERS
	    	 ************************/
	    	for (Order o : orders) { 
	    		
	    		// Probably smart to validate orders
	    		if (	o.src >= 0 && 
	    				o.dest >= 0 &&
	    				o.src != o.dest &&
	    				pw.GetPlanet(o.src).Owner() == 1 && 
			    		o.numShips > 0 && 
			    		o.numShips <= pw.GetPlanet(o.src).NumShips()) {
		    			
		    		pw.IssueOrder(o.src, o.dest, o.numShips);
	    		}
	    	}
	    	//*/

	    	
	
			// If enemy has no planets and is basically dead. Might be premature! :D
			if (pw.EnemyPlanets().isEmpty()) System.err.println("All your base are belong to us!");
			
    	} // Player 1 isAlive()
    	else System.err.println("GG");
	
    }
    
    
    
    

    public static void main(String[] args) {
    	String line = "";
		String message = "";
		int c;
		try {
		    while ((c = System.in.read()) >= 0) {
			switch (c) {
			case '\n':
			    if (line.equals("go")) {
			    	PlanetWars pw = new PlanetWars(message, args);
					DoTurn(pw, args);
					pw.FinishTurn();
					message = "";
			    } else {
				message += line + "\n";
			    }
			    line = "";
			    break;
			default:
			    line += (char)c;
			    break;
			}
		    }
		} catch (Exception e) {
		    // Owned.
		}
    }
}

