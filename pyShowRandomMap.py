﻿import subprocess, sys, random
from random import randint
import time
import datetime

def main():
    
    st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    
    map = int(randint(1, 101))
    
    callA = str(sys.argv[1])
    callB = str(sys.argv[2])
    
    botA = callA[:callA.find(".")]
    botB = callB[:callB.find(".")]
    
    # Use this if first bot is HekeBot
    java_heke_file = open("HekeBotMatchData.txt", "w")
    java_heke_file.write(st + '\n' + botA + ' vs ' + botB + ' - Map ' + str(map) + '\n')
    java_heke_file.close()
    #

    pipe = subprocess.Popen('java -jar PlayGame.jar maps/map' + str(map) + '.txt 1000 1000 log.txt "java -jar bots/' + callA + '" "java -jar bots/' + callB + '" | java -jar ShowGame.jar', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
    # pipe = subprocess.Popen('java -jar PlayGame.jar maps/map' + str(map) + '.txt 1000 1000 log.txt "java -jar bots/' + callA + '" "java -jar bots/' + callB + '" | python visualizer/visualize_localy.py', stderr=subprocess.PIPE, shell=True, universal_newlines=True)
    
    out, err = pipe.communicate()
    
    p = str(err)
    
    # print (str(out))
    
    
    text_file = open("py_RandomMapMatchLog.txt", "w")
    text_file.write(st + "\n" + out)
    text_file.close()
    
    print ("Map " + str(map))
    # Figure out who won
    if p.find("Player 1 Wins") == -1:
        if p.find("Player 2 Wins") == -1:
            print ("Draw...")
            return 3
        else:
            print (botB + " won")
            return 2
    else:
        print (botA + " won")
        return 1

if __name__ == "__main__":
    main()