﻿import subprocess, sys, random
from random import randint
import time
import datetime

def match(botA, botB, callA, callB, mapnumber, replay):

    '''
    Alan ymmärtää tätä Popen PIPE stdout stderr kuvioo.
    - stdout=subprocess.PIPE sisältää sen pelidatan, joka tarvitaan, että pelin voi näyttää Visualizerilla.
    - stderr=subprocess.PIPE puolestaan sisältää sen mitä pelistä tulostetaan ulos ja minkä avulla selviää mm. pelin voittaja.
    
    JOS poimin stdout talteen esim. näin
        - stdout=subprocess.PIPE
    niin se pelidata tulee poimittua ja voidaan tallentaa txt-tiedostoon.
    
    JOS poimin stderr talteen esim. näin
        - stderr=subprocess.PIPE
    niin tätä tietoa voin käyttää selvittämään kumpi voitti matsin. Voin myös tallentaa tämän datan txt-tiedostoon analysoitavaksi.
    
    Mutta miten saada yhtäaikaa pelidata txt-tiedostoon ja tulos printattua ja mahdollisesti myös txt-tiedostoon?
    
    Näköjään, jos koitan yhtäaikaa poimia 
        - stdout=subprocess.PIPE, stderr=subprocess.PIPE 
    niin koodi jää jumiin. Ei tule erroria. Onko tää se Pipe Overflow?
    
    Jos taas käytän tätä 
        - stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    Niin näköjään pelin tulos löytyy printtiin ja lisäksi sekä pelidata että pelin tuloste voidaan kirjoittaa tiedostoon, mutta ne ovat sekottuneet ristiin ja siitä on vaikea parsia sitä varsinaista Playbackia.
    
    Kuinka ne saataisiin eroteltua eri muuttujiin? 
    Vastaus on 
        - communicate()
    Näköjään noi putket menee jotenkin jumiin, jos koittaa yhtäaikaa noita ja communicate() on just sitä varten olemassa. Cool. :)
    
    Vielä yksi huomio! Jos käyttää
        - | java -jar ShowGame.jar
    tai muuten ton | avulla nappaa ilmeisesti sen outputin niin se ei tule silloin enää stdout:iin, edes communicate():lla. Hämyä... Ei voi sekä visualisoida suoraan, että tallentaa pelidataa myöhemmäksi...
    
    '''
    
    # timestamp
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    timestamp_file = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H-%M-%S')
    
    # Use this if first bot is HekeBot
    java_heke_file = open("HekeBotMatchData.txt", "w")
    java_heke_file.write(timestamp + '\n' + botA + ' vs ' + botB + ' - Maps: ' + str(mapnumber) + '\n')
    java_heke_file.close()
    #
    
    # Just to print out what we are calling...
    call = 'java -jar PlayGame.jar maps/map' + str(mapnumber) + '.txt 1000 300 log.txt "java -jar bots/' + callA + '" "java -jar bots/' + callB + '"'
    
    # For calling bots with parameters
    # pipe = subprocess.Popen('java -jar PlayGame.jar maps/map' + str(mapnumber) + '.txt 500 300 log.txt "java -jar bots/' + callA + '" "java -jar bots/' + callB + '"', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
    pipe = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
    
    out, err = pipe.communicate()
    p = str(err)
    
    
    log_file = open("py_MatchUpLog.txt", "a")
    log_file.write(timestamp + '\n' + call + '\n');
    
    # Figure out who won and on which turn
    lastturncharnumber = p.rfind("Turn ")       # At what index starts the last Turn substring
    turnstrend = p[lastturncharnumber + 5:]
    turnstr = turnstrend[:turnstrend.find("\n")]
    turnnumber = int(turnstr)
    
    result = 0
    b_draw = "0"
    # And see if opponent timed out
    if p.find("timed out.") == -1:  # If no time out
        if p.find("Player 1 Wins") == -1:
            if p.find("Player 2 Wins") == -1:
                print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t Draw...")
                result = 5
                b_draw = "1"
            else:
                print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t " + botB + " won")
                result = 2
        else:
            print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t " + botA + " won")
            result = 1
    else:   # If time out
        if p.find("Player 1 Wins") == -1:
            if p.find("Player 2 Wins") == -1:
                print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t Draw... (time out)")
                result = 6
                b_draw = "1"
            else:
                print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t " + botB + " won (opponent timed out)")
                result = 4
        else:
            print ("Map " + str(mapnumber) + ".  \tTurn " + str(turnnumber) + " \t " + botA + " won (opponent timed out)")
            result = 3

    str_timeout = ''
    if result == 3 | result == 4:
        str_timeout = ' Timed out'
        
    # To print what happened in the game
    # print (err)
    err_file_name = timestamp_file + '_map' + str(mapnumber) + ' err log'
    
    err_file = open("logs/" + err_file_name + str_timeout + '.txt', 'w')
    err_file.write(timestamp + '\n' + call + '\n');
    err_file.write(p + '\n\n')
    err_file.close()
    
    # To write a replay-file
    '''
    Keksin miten saada tallennettua kaikkien pelien replayt näppärästi eri nimellä ja "oikeessa" formaatissa. Paitsi, että formaatissa on ongelmana, että tää Python haluaa väkisin lisätä sinne CR (Carriage Return) merkin, mikä puolestaan ei kelpaa Visualizerille...
    
    Ja kun yritän kirjoittaa tiedoston Binääri-muodossa, jolloin noi CR:t välttäisi, niin valittaa, että bufferi ei tue str:ää, eli ilmeisesti stringii. 
    
    Vois luovuttaa täl erää. Notepad++:lla voi kuitenkin Ctrl+H ja \r korvata noi CR:t tyhjällä ja sit se tiedosto toimii replayna.
    '''
    log_file.write("Writing Replay? :: " + str(replay) + " Result was: " + str(result))
    
    
    if replay == 1:
              
        replay_file_name = timestamp_file + '_map' + str(mapnumber) + '_' + botA + '_vs_' + botB
        
        log_file.write("Starting to open replay-file... "+replay_file_name)
        
        replay_file = open("replays/"+replay_file_name, "wb")
        # replay_file = open("replays/"+replay_file_name, "w")
        # temp_file = open("replays/temp", "w")
        
        log_file.write("Replay-file opened!")
        
        replay_file.write(bytes("game_id=0\nwinner=0\nloser=0\nmap_id="+str(mapnumber)+"\ndraw="+str(b_draw)+"\ntimestamp="+timestamp+"\nplayer_one="+botA+"\nplayer_two="+botB+"\nplayback_string="+out, 'UTF-8'))
        # replay_file.write("game_id=0\nwinner=0\nloser=0\nmap_id="+str(mapnumber)+"\ndraw="+str(b_draw)+"\ntimestamp="+timestamp+"\nplayer_one="+botA+"\nplayer_two="+botB+"\nplayback_string="+out)
        # temp_file.write("game_id=0\nwinner=0\nloser=0\nmap_id="+str(mapnumber)+"\ndraw="+str(b_draw)+"\ntimestamp="+timestamp+"\nplayer_one="+botA+"\nplayer_two="+botB+"\nplayback_string="+out)
        
        replay_file.close()
        # replay_file.close()
        # temp_file.close()
        
        '''
        replay_file.write("game_id=0\n")
        replay_file.write("winner=0\n")
        replay_file.write("loser=0\n")
        replay_file.write("map_id="+str(mapnumber)+"\n")
        replay_file.write("draw="+str(draw)+"\n")
        replay_file.write("timestamp="+timestamp+"\n")
        replay_file.write("player_one="+botA+"\n")
        replay_file.write("player_two="+botB+"\n")
        replay_file.write("playback_string="+out)
        
        
        
        rf = open("temp", "w")
        temp = "game_id=0\nwinner=0\nloser=0\nmap_id="+str(mapnumber)+"\ndraw="+draw+"\nplayer_one="+botA+"\nplayer_two="+botB+"\nplayback_string="+out
        rf.write(temp)
        rf.close()
        
        
        rf = open("replays/temp", "r")
        replay_file = open("replays/"+replay_file_name, "wb")
        for line in rf:
            newline = line.rstrip('\r\n')
            replay_file.write(bytes(newline, 'UTF-8'))
            replay_file.write(bytes('\n', 'UTF-8')) # last line of script
        
        rf.close()
        replay_file.close()
        
        
        time.sleep(1)
        '''
        
    log_file.close()
    
    
    return (result, turnnumber)


    
def main():
    
    ''' Parameters:
    1 = call command for bot A
    2 = call command for bot B
    3 = number of maps to play
    4 = Start map 1-100 or 0 to start from random map
    5 = Number of matches per map or 0 if play mirrored match for every map
    6 = 1 if save replay file
    
    for argument in sys.argv[1:]:
        print (argument)
    '''
    
    if len(sys.argv) < 3:
        print ("Give parameters:\ncall command for bot A\ncall command for bot B\nnumber of maps to play\ntrue if start from random map")
        return
    
    # Set Bot names
    callA = str(sys.argv[1])
    callB = str(sys.argv[2])
    print ("\nPlayer 1:\t " + callA)
    print ("Player 2:\t " + callB)
    
    botA = callA[:callA.find(".")]
    botB = callB[:callB.find(".")]
    
    ''' Getting out the Bot-name, test
    
    callbots = "callA = " + callA + "\ncallB = " + callB + "\nbotA = " + botA + "\nbotB = " + botB
    callbots_file = open("CallBots.txt", "w")
    callbots_file.write(callbots)
    callbots_file.close()
    '''
            
    # Game Stats
    matches = 0
    matchesWon = [0.]*len(sys.argv)
    timeOuts = [0.]*len(sys.argv)
    draws = 0
    
    # Player numbers
    a = 1
    b = 2
    
    # PRINT OUT MATCH UP PARAMETERS
    
    # Maps
    numofmaps = 1
    if len(sys.argv) > 3:
        if int(len(sys.argv)) < 1 & int(len(sys.argv)) > 100:
            print ("There are currently only 100 maps.\nGive Number of Maps between 1 and 100.\nNumber of Maps default is 1 if no parameter is given.")
            return
        else:
            numofmaps = int(sys.argv[3])
    print ("Maps:\t\t " + str(numofmaps))
    
    startmap = 1
    if len(sys.argv) > 4:
        if int(sys.argv[4]) == 0:    # 0 = Random
            startmap = int(randint(1, 100))
            print ("Starting Map:\t Random")
        elif int(sys.argv[4]) > 100 | int(sys.argv[4]) < 0:
            print ("There are currently only 100 maps.\nGive Starting Map number between 1 and 100 or 0 for Random Map.\nStarting Maps number default is 1 if no parameter is given.")
            return
        else:
            startmap = int(sys.argv[4])
            print ("Starting Map:\t " + sys.argv[4])
    else:
        startmap = 1
        print ("Starting Map:\t 1")
    
    matchesToPlay = 0
    if len(sys.argv) > 5:
        if int(sys.argv[5]) == 0:    # 0 = mirrored
            matchesToPlay = 0
            print ("# of Matches:\t Mirrored (2)")
        elif int(sys.argv[5]) > 10000 | int(sys.argv[5]) < 0:
            print ("Please give a positive number of matches to play and no more than 10000.\nOr 0 for playing mirrored matches on each map.")
            return
        else:
            matchesToPlay = int(sys.argv[5])
            print ("# of Matches:\t " + str(matchesToPlay))
    else:
        matchesToPlay = 1
        print ("# of Matches:\t " + str(matchesToPlay))
    
    replay = 0
    if len(sys.argv) > 6:
        if int(sys.argv[6]) == 1:    # 1 = true
            replay = 1
            print ("Save Replay?\t Yes")
        else:
            print ("Save Replay?\t No")
    else:
        print ("Save Replay?\t No")
        
    print ("\n")
     




     
    # Writing log files
    st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    
    # Not needed probably...
    log_file = open("py_MatchUpLog.txt", "w")
    # For printin out arguments, testing calling from java
    for argument in sys.argv[1:]:
        log_file.write(argument + '\n')
    log_file.close()
    
    
    
    matches_file = open("py_Matches.txt", "w")
    matches_file.write(st + '\n' + botA + ' vs ' + botB + '\n')
    
    
    # String to write all the match results of this match up
    matches_results = ""
    
    # Read and store previous matches
    matchup_file = open("py_MatchUpResults.txt", "r+")
    previous_matchups = matchup_file.read()
    matchup_file.close()
    
    # String to write the total result of this match up
    thismatch = st + '\n' + botA + ' vs ' + botB + ' - Maps: ' + str(startmap) + ' - ' + str(startmap + numofmaps -1) + '\n'
       
    
    
    # Going through the maps
    for formap in range(startmap, startmap + numofmaps):
        
        if formap > 100:
            map = formap - 100
        else:
            map = formap
            
        # Play a match
        if matchesToPlay == 0:
            forhowmany = 1
        else:
            forhowmany = matchesToPlay
        for formatch in range(1, forhowmany+1):
            result, turns = match(botA, botB, callA, callB, map, replay)
            if result == 1:
                matchesWon[1] += 1
                result_str = botA
            elif result == 2:
                matchesWon[2] += 1
                result_str = botB
            elif result == 3:
                matchesWon[1] += 1
                timeOuts[2] += 1
                result_str = botA + " (opponent timed out)"
            elif result == 4:
                matchesWon[2] += 1
                timeOuts[1] += 1
                result_str = botB + " (opponent timed out)"
            elif result == 5:
                draws += 1
                result_str = "Draw..."
            else:
                draws += 1
                result_str = "Draw... (time out)"

            matches += 1
            
            matches_results = matches_results + "Map " + str(map) + " \t: " + result_str + "\n"
        
        if matchesToPlay == 0:
        
            # Switch player "sides"
            # Play a match
            result = match(botB, botA, callB, callA, map, replay)
            if result == 1:
                matchesWon[2] += 1
                result_str = botB
            elif result == 2:
                matchesWon[1] += 1
                result_str = botA
            elif result == 3:
                matchesWon[2] += 1
                timeOuts[1] += 1
                result_str = botA + " (opponent timed out)"
            elif result == 4:
                matchesWon[1] += 1
                timeOuts[2] += 1
                result_str = botB + " (opponent timed out)"
            else:
                draws += 1
                result_str = "Draw..."

            matches += 1
            
            matches_results = matches_results + "Map " + str(map) + " (s)\t: " + result_str + "\n"
        
    # 
    
    # Posting and writing results
    # matchup_file = open("py_MatchUpResults.txt", "a")
    for i in range (1, 3):
        # if matches[i] != 0:
        if matches != 0:
            if i == 1:
                player = botA
            else:
                player = botB
            
            winperc = int(100 * matchesWon[i] / matches) / 100
            thismatch = thismatch + ("Games: " + str(int(matches)) + " : win-loss(timeout): " + str(int(matchesWon[i])) + "-" + str(int(matches - int(matchesWon[i]) - int(draws))) + "(" + str(int(timeOuts[i])) + ") : " + str(winperc) + " % won. " + player + ".\n")
            # thismatch = thismatch + ("Games: " + str(int(matches[i])) + " : win-loss: " + str(int(matchesWon[i])) + "-" + str(int(matches[i] - matchesWon[i] - draws)) + " : " + str(matchesWon[i] / matches[i]) + " % won. " + player + ".\n")
    
    thismatch = thismatch + "Draws: " + str(draws) + "\n"
    print ('\n' + thismatch)
    
    matchup_file = open("py_MatchUpResults.txt", "w")
    matchup_file.write(thismatch + '\n\n' + previous_matchups)
    matchup_file.close()
    
    matches_file = open("py_Matches.txt", "a")
    matches_file.write(matches_results)
    matches_file.close()
    
if __name__ == "__main__":
    main()